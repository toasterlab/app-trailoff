export default {
  art: {width: 149, height: 116},
  expand: {width: 350, height: 615},
  headshot: {width: 203, height: 204},
  icon: {width: 43, height: 43},
  map: {width: 327, height: 140},
  progress_00: {width: 247, height: 504},
  progress_01: {width: 247, height: 504},
  progress_02: {width: 247, height: 504},
  progress_03: {width: 247, height: 504},
  progress_04: {width: 247, height: 504},
  progress_05: {width: 247, height: 504},
  progress_06: {width: 247, height: 504},
  progress_07: {width: 247, height: 504},
  progress_08: {width: 247, height: 504},
  progress_09: {width: 247, height: 504},
  progress_10: {width: 247, height: 504},
  progress_11: {width: 247, height: 504},
  progress_12: {width: 247, height: 504},
  shape: {width: 239, height: 488},
};
