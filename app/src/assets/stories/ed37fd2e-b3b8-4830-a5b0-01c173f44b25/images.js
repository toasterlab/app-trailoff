// THIS IS A GENERATED FILE, DO NOT EDIT
// (run yarn rebuildImageLibrary instead)

import info from './images_info.js';
export default {
  info,
  art: require('./art.png'),
  expand: require('./expand.png'),
  headshot: require('./headshot.png'),
  icon: require('./icon.png'),
  map: require('./map.png'),
  progress_00: require('./progress_00.png'),
  progress_01: require('./progress_01.png'),
  progress_02: require('./progress_02.png'),
  progress_03: require('./progress_03.png'),
  progress_04: require('./progress_04.png'),
  progress_05: require('./progress_05.png'),
  progress_06: require('./progress_06.png'),
  progress_07: require('./progress_07.png'),
  progress_08: require('./progress_08.png'),
  progress_09: require('./progress_09.png'),
  progress_10: require('./progress_10.png'),
  progress_11: require('./progress_11.png'),
  progress_12: require('./progress_12.png'),
  shape: require('./shape.png'),
};
