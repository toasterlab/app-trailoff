import data_0 from './01f53fe9-9ae7-487b-8362-2f7636050dff/content.json';
import data_1 from './081c3d35-27d8-4a9c-ba8f-ac8ab7c7735e/content.json';
import data_2 from './0b9b1fd5-4cb7-4724-8fdc-c85121aaa30f/content.json';
import data_3 from './0dbfab2b-1789-4399-a94e-0e94efa54fc7/content.json';
import data_4 from './49386444-c777-4200-b0f7-02080e95999a/content.json';
import data_5 from './aa29715a-3b92-4351-a3d8-ba7ea55eddf9/content.json';
import data_6 from './c88e373f-2b6d-48b5-b091-f35e9fcd4681/content.json';
import data_7 from './cc4ee685-088b-4899-9dc2-5c24f25fcc9d/content.json';
import data_8 from './ed37fd2e-b3b8-4830-a5b0-01c173f44b25/content.json';
import data_9 from './ed92dae2-b45b-4903-a30a-a1587d06147f/content.json';

export default [
  {
    order: 0,
    id: 0,
    author: 'afaq',
    coauthor: '',
    trail: 'Cooper River Trail',
    title: 'The Way Sand Wants for Water',
    color: '#7192B8',
    uuid: '0dbfab2b-1789-4399-a94e-0e94efa54fc7',
    detail: data_3
  },
  {
    order: 1,
    id: 1,
    author: 'Ari',
    coauthor: '',
    trail: 'Chester Valley Trail',
    title: "Where the Light Won't Go",
    color: '#D03B02',
    uuid: '081c3d35-27d8-4a9c-ba8f-ac8ab7c7735e',
    detail: data_1
  },
  {
    order: 2,
    id: 2,
    author: 'Carmen Maria Machado',
    coauthor: '',
    trail: 'Schuylkill River Trail',
    title: 'River Devil II: The Return',
    color: '#3097C3',
    uuid: '49386444-c777-4200-b0f7-02080e95999a',
    detail: data_4
  },
  {
    order: 3,
    id: 3,
    author: 'Denise Valentine',
    coauthor: '',
    trail: 'Tacony Creek Trail',
    title: 'Deeply Routed',
    color: '#F9A98E',
    uuid: 'aa29715a-3b92-4351-a3d8-ba7ea55eddf9',
    detail: data_5
  },
  {
    order: 4,
    id: 4,
    author: 'donia salem harhoor',
    coauthor: '',
    trail: 'Perkiomen Trail',
    title: "A Sycamore's Psalm",
    color: '#8AC094',
    uuid: 'ed92dae2-b45b-4903-a30a-a1587d06147f',
    detail: data_9
  },
  {
    order: 5,
    id: 5,
    author: 'Eppchez !',
    coauthor: '',
    trail: 'Delaware River Trail South',
    title: 'Appear To Me',
    color: '#7B5FB9',
    uuid: '01f53fe9-9ae7-487b-8362-2f7636050dff',
    detail: data_0
  },
  {
    order: 6,
    id: 6,
    author: 'Erin McMillon',
    coauthor: '',
    trail: 'D&L Trail',
    title: 'The Inside Outside',
    color: '#5FA211',
    uuid: 'c88e373f-2b6d-48b5-b091-f35e9fcd4681',
    detail: data_6
  },
  {
    order: 7,
    id: 7,
    author: 'Jacob Camacho',
    coauthor: 'with help from Trinity Norwood',
    trail: 'Kensington & Tacony Trail',
    title: 'Conversations',
    color: '#FFE051',
    uuid: 'cc4ee685-088b-4899-9dc2-5c24f25fcc9d',
    detail: data_7
  },
  {
    order: 8,
    id: 8,
    author: 'Jacob Winterstein',
    coauthor: '',
    trail: 'John Heinz Refuge Trail',
    title: 'The Land Remembers',
    color: '#FBA41E',
    uuid: '0b9b1fd5-4cb7-4724-8fdc-c85121aaa30f',
    detail: data_2
  },
  {
    order: 9,
    id: 9,
    author: 'Li Sumpter',
    coauthor: '',
    trail: "Schuylkill River Trail: Bartram's Mile",
    title: 'Chronicles of Asylum',
    color: '#BE4187',
    uuid: 'ed37fd2e-b3b8-4830-a5b0-01c173f44b25',
    detail: data_8
  }
];
