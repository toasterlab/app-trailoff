// THIS IS A GENERATED FILE, DO NOT EDIT
// (run yarn rebuildSoundLibrary instead)

import s_0 from './01f53fe9-9ae7-487b-8362-2f7636050dff/sounds.js';
import s_1 from './081c3d35-27d8-4a9c-ba8f-ac8ab7c7735e/sounds.js';
import s_2 from './0b9b1fd5-4cb7-4724-8fdc-c85121aaa30f/sounds.js';
import s_3 from './0dbfab2b-1789-4399-a94e-0e94efa54fc7/sounds.js';
import s_4 from './49386444-c777-4200-b0f7-02080e95999a/sounds.js';
import s_5 from './aa29715a-3b92-4351-a3d8-ba7ea55eddf9/sounds.js';
import s_6 from './c88e373f-2b6d-48b5-b091-f35e9fcd4681/sounds.js';
import s_7 from './cc4ee685-088b-4899-9dc2-5c24f25fcc9d/sounds.js';
import s_8 from './ed37fd2e-b3b8-4830-a5b0-01c173f44b25/sounds.js';
import s_9 from './ed92dae2-b45b-4903-a30a-a1587d06147f/sounds.js';

export default {
  '01f53fe9-9ae7-487b-8362-2f7636050dff': s_0,
  '081c3d35-27d8-4a9c-ba8f-ac8ab7c7735e': s_1,
  '0b9b1fd5-4cb7-4724-8fdc-c85121aaa30f': s_2,
  '0dbfab2b-1789-4399-a94e-0e94efa54fc7': s_3,
  '49386444-c777-4200-b0f7-02080e95999a': s_4,
  'aa29715a-3b92-4351-a3d8-ba7ea55eddf9': s_5,
  'c88e373f-2b6d-48b5-b091-f35e9fcd4681': s_6,
  'cc4ee685-088b-4899-9dc2-5c24f25fcc9d': s_7,
  'ed37fd2e-b3b8-4830-a5b0-01c173f44b25': s_8,
  'ed92dae2-b45b-4903-a30a-a1587d06147f': s_9,
};
