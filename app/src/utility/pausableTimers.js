// Based on: https://gist.github.com/ncou/3a0a1f89c8e22416d0d607f621a948a9
export default {
  setInterval: (callback, delay) => {
    let timer = {
      timerId: null,
      start: delay,
      duration: delay,
      remaining: delay
    };

    if (typeof callback !== 'function') {
      console.error('TIMER ERROR: Callback must be a function');
      return false;
    }

    timer.pause = () => {
      // DEBUG: console.log(`INT TIMER (${timer.timerId}): PAUSE `);
      clearTimeout(timer.timerId);
      timer.remaining -= new Date() - timer.start;
      return true;
    };

    timer.pausedAt = () => {
      return timer.remaining;
    };

    timer.resume = () => {
      timer.start = new Date();
      // DEBUG: console.log(`INT TIMER (${timer.timerId}): RESUME `);
      timer.timerId = setTimeout(() => {
        timer.remaining = delay;
        timer.resume();
        callback();
      }, timer.remaining);
      return true;
    };

    timer.cancel = () => {
      clearTimeout(timer.timerId);
      timer = {};
      return true;
    };

    timer.resume();
    return timer;
  },

  setTimeout: (callback, delay) => {
    let timer = {
      timerId: null,
      start: delay,
      remaining: delay,
      duration: delay,
      isComplete: false
    };

    if (typeof callback !== 'function') {
      console.error('TO TIMER ERROR: Callback must be a function');
      return false;
    }

    timer.pause = () => {
      //DEBUG: console.log(`TO TIMER (${timer.timerId}): PAUSE `);
      window.clearTimeout(timer.timerId);
      timer.remaining -= new Date() - timer.start;
      return true;
    };

    timer.pausedAt = () => {
      return timer.remaining;
    };

    timer.isFinished = () => {
      return timer.isComplete;
    };

    timer.cancel = () => {
      //DEBUG: console.log(`TO TIMER (${timer.timerId}): CANCELLED `);
      clearTimeout(timer.timerId);
      timer.timer = {};
      return true;
    };

    timer.resume = () => {
      //DEBUG: console.log(`TO TIMER (${timer.timerId}): RESUME `);
      timer.start = new Date();
      window.clearTimeout(timer.timerId);
      timer.timerId = window.setTimeout(() => {
        callback();
        //DEBUG: console.log(`TO TIMER (${timer.timerId}): DONE `);
        timer.isComplete = true;
      }, timer.remaining);
      //DEBUG: console.log(`TO TIMER (${timer.timerId}): INIT ${delay} `);
      return true;
    };

    timer.resume();
    return timer;
  }
};
