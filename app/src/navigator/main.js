import React, { useEffect, useState } from 'react';
import Orientation from 'navigator/orientation';
import Normal from 'navigator/normal';
import { useSelector, useDispatch } from 'react-redux';
import { onAppLaunch, onAppStateTransition } from 'state/actions';
import { AppState } from 'react-native';

const useMountEffect = f => useEffect(f, []);

export default ({ route, navigation }) => {
  const isInitialLaunch = useSelector(
    state => state.settings.app.isInitialLaunch
  );
  const dispatch = useDispatch();

  useMountEffect(() => {
    dispatch(onAppLaunch());
  });

  useEffect(() => {
    const unsubscribe = AppState.addEventListener('change', e => {
      if (e === 'active') {
        dispatch(onAppStateTransition('FOREGROUND'));
      } else if (e === 'background' || e === 'inactive') {
        dispatch(onAppStateTransition('BACKGROUND'));
      }
    });
    return unsubscribe;
  });

  if (isInitialLaunch) {
    return <Orientation />;
  } else {
    return <Normal />;
  }
};
