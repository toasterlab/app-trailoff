import React, { useEffect } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import StoryListing from 'navigator/storyList';
import About from 'screens/about';
import Howto from 'screens/howto';
import AppCredits from 'screens/appCredit';
import Connect from 'screens/connect';
import Support from 'screens/support';
import Orientation from 'navigator/orientation.js';
import Settings from 'screens/settings';
import CustomDrawer from './customDrawer';
const Drawer = createDrawerNavigator();

export default ({ route, navigation }) => {
  return (
    <Drawer.Navigator drawerContent={CustomDrawer}>
      <Drawer.Screen name="Stories" component={StoryListing} />
      <Drawer.Screen name="About" component={About} />
      <Drawer.Screen name="How To" component={Howto} />
      <Drawer.Screen name="Credits" component={AppCredits} />
      <Drawer.Screen name="Connect" component={Connect} />
      <Drawer.Screen name="Support" component={Support} />
      <Drawer.Screen name="Orientation" component={Orientation} />
      <Drawer.Screen name="Settings" component={Settings} />
    </Drawer.Navigator>
  );
};
