import React from 'react';
import Home from 'navigator/home.js';
import {
  Story,
  StoryAuthor,
  StoryCredit,
  StoryExpand,
  StoryProgress,
  StoryLetsGo,
} from 'screens/index.js';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();
export default () => {
  return (
    <Stack.Navigator headerMode="modal">
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Story" component={Story} />
      <Stack.Screen name="StoryAuthor" component={StoryAuthor} />
      <Stack.Screen name="StoryCredit" component={StoryCredit} />
      <Stack.Screen name="StoryExpand" component={StoryExpand} />
      <Stack.Screen name="StoryProgress" component={StoryProgress} />
      <Stack.Screen name="StoryLetsGo" component={StoryLetsGo} />
    </Stack.Navigator>
  );
};
