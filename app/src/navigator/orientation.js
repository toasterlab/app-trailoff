import React from 'react';
import Normal from 'navigator/normal.js';
import {
  Orientation1,
  Orientation2,
  Orientation3,
  Orientation4,
  Orientation5,
} from 'screens/index.js';
import Heap from '@heap/react-native-heap';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator headerMode="modal">
      <Stack.Screen name="Orientation1" component={Orientation1} />
      <Stack.Screen name="Orientation2" component={Orientation2} />
      <Stack.Screen name="Orientation3" component={Orientation3} />
      <Stack.Screen name="Orientation4" component={Orientation4} />
      <Stack.Screen name="Orientation5" component={Orientation5} />
      <Stack.Screen name="Normal" component={Normal} />
    </Stack.Navigator>
  );
};
