import * as React from 'react';
import {ImageBackground, StyleSheet} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import fonts from 'styles/fonts';
export default props => {
  return (
    <ImageBackground
      source={require('assets/common/sidebar.png')}
      style={styles.backgroundImage}>
      <DrawerContentScrollView {...props}>
        <DrawerItemList
          inactiveBackgroundColor="transparent"
          activeBackgroundColor="rgba(0, 0, 0, .5)"
          labelStyle={styles.labelStyle}
          style={styles.drawerItemList}
          {...props}
        />
      </DrawerContentScrollView>
    </ImageBackground>
  );
};
const styles = StyleSheet.create({
  backgroundImage: {width: '100%', height: '100%'},
  drawerItemList: {
    textTransform: 'uppercase',
    fontFamily: fonts.hype,
    fontSize: 30,
    margin: 0,
    marginBottom: -19,
    padding: 0,
  },
  labelStyle: {
    fontSize: 25,
    fontWeight: '100',
    color: '#ffffff',
    fontFamily: fonts.hype,
    textTransform: 'uppercase',
    textAlign: 'right',
    alignSelf: 'stretch',
  },
});
