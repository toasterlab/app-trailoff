import React from 'react';
import List from 'screens/list';
import Art from 'screens/art';
import Map from 'screens/map';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

export default () => {
  return (
    <Stack.Navigator headerMode="modal">
      <Stack.Screen name="List" component={List} />
      <Stack.Screen name="Art" component={Art} />
      <Stack.Screen name="Map" component={Map} />
    </Stack.Navigator>
  );
};
