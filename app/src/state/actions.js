function action(type, payload = {}) {
  return { type, payload };
}

export const SET_CURRENT_STORYID = 'SET_CURRENT_STORYID';
export const UPDATE_CURRENT_STORYID = 'UPDATE_CURRENT_STORYID';
export const PERMISSIONS_UPDATE = 'PERMISSIONS_UPDATE';
export const PERMISSIONS_UPDATE__R = 'PERMISSIONS_UPDATE__R';
export const PERMISSIONS_REQUEST = 'PERMISSIONS_REQUEST';
export const setCurrentStoryIdx = payload =>
  action(SET_CURRENT_STORYID, payload);
export const requestPermissions = payload =>
  action(PERMISSIONS_REQUEST, payload);

export const INCOMING_LOCATION_UPDATE = 'INCOMING_LOCATION_UPDATE';

export const APP_DEBUG_MODE = 'APP_DEBUG_MODE';
export const toggleDebugMode = () => action(APP_DEBUG_MODE);

export const APP_ENABLE_LOCATION_WATCH = 'APP_ENABLE_LOCATION_WATCH';
export const APP_SET_SESSION_PROGRESS = 'APP_SET_SESSION_PROGRESS';
export const APP_SET_SESSION_PROGRESS_INDICATOR = 'APP_SET_SESSION_PROGRESS_INDICATOR';
export const APP_SET_SESSIONID = 'APP_SET_SESSIONID';
export const APP_CLEAR_SESSIONID = 'APP_CLEAR_SESSIONID';
export const APP_LOCATION_UPDATE = 'APP_LOCATION_UPDATE';
export const APP_LOCATION_TOGGLE_FAKE = 'APP_LOCATION_TOGGLE_FAKE';
export const APP_LOCATION_UPDATE_FAKE = 'APP_LOCATION_UPDATE_FAKE';
export const APP_LOCATION_SET = 'APP_LOCATION_SET';
export const updateProgressIndicator = payload => action(APP_SET_SESSION_PROGRESS_INDICATOR, payload);
export const updateFakeLocation = payload => action(APP_LOCATION_UPDATE_FAKE, payload);
export const toggleFakeLocation = payload => action(APP_LOCATION_TOGGLE_FAKE, payload);
export const useFakeLocation = payload => action(APP_LOCATION_SET, payload);

export const APP_MUTE_UI = 'APP_MUTE_UI';
export const APP_REQUEST_LOCATION = 'APP_REQUEST_LOCATION';
export const NETWORK_STATE_CHANGE = 'NETWORK_STATE_CHANGE';
export const APP_LAUNCH = 'APP_LAUNCH';
export const APP_ERROR = 'APP_ERROR';
export const APP_ERROR_NONFATAL = 'APP_ERROR_NONFATAL';
export const APP_FOOTER_BUTTON_CLICK = 'APP_FOOTER_BUTTON_CLICK';
export const APP_STATE_TRANSITION = 'APP_STATE_TRANSITION';
export const onAppStateTransition = payload =>
  action(APP_STATE_TRANSITION, payload);
export const onAppLaunch = () => action(APP_LAUNCH);
export const onFooterButtonClick = payload =>
  action(APP_FOOTER_BUTTON_CLICK, payload);
export const muteUiSounds = payload => action(APP_MUTE_UI, payload);

export const INCREMENT_LAUNCH_COUNT = 'INCREMENT_LAUNCH_COUNT';

export const STORY_GET_MANIFEST__R = 'STORY_GET_MANIFEST__R';
export const STORY_GET_MANIFEST = 'STORY_GET_MANIFEST';
export const STORY_UPDATE_WEATHER = 'STORY_UPDATE_WEATHER';
export const WEATHER_UPDATE_ALL = 'WEATHER_UPDATE_ALL';
export const STATUS_UPDATE_ALL = 'STATUS_UPDATE_ALL';
export const WEATHER_UPDATE = 'WEATHER_UPDATE';
export const STATUS_UPDATE = 'STATUS_UPDATE';
export const INCOMING_STATUS_UPDATE = 'INCOMING_STATUS_UPDATE';

export const PKG_CHECK_FOR_UPDATES = 'PKG_CHECK_FOR_UPDATES';
export const PKG_DOWNLOAD = 'PKG_DOWNLOAD';
export const PKG_DOWNLOAD_CANCEL = 'PKG_DOWNLOAD_CANCEL';
export const PKG_REMOVE = 'PKG_REMOVE';
export const PKG_UNPACK_COMPLETE = 'PKG_UNPACK_COMPLETE';
export const packageRemove = payload => action(PKG_REMOVE, payload);
export const packageDownload = payload => action(PKG_DOWNLOAD, payload);
export const packageDownloadCancel = payload =>
  action(PKG_DOWNLOAD_CANCEL, payload);

export const APP_DIALOG = 'APP_DIALOG';
export const appDialog = payload => action(APP_DIALOG, payload);

export const UPDATE_STATUS = 'UPDATE_STATUS';

export const DOWNLOAD_STATUS = 'DOWNLOAD_STATUS';

export const PLAYBACK_TOGGLE_ONEWAY = 'PLAYBACK_TOGGLE_ONEWAY';
export const PLAYBACK_RESET = 'PLAYBACK_RESET';
export const PLAYBACK_RESET_QUEUE = 'PLAYBACK_RESET_QUEUE';
export const PLAYBACK_GO = 'PLAYBACK_GO';
export const PLAYBACK_PAUSE = 'PLAYBACK_PAUSE';
export const PLAYBACK_RESUME = 'PLAYBACK_RESUME';
export const PLAYBACK_REWIND = 'PLAYBACK_REWIND';
export const PLAYBACK_HALT = 'PLAYBACK_HALT';
export const PLAYBACK_TRACK_ENQUEUE = 'PLAYBACK_TRACK_ENQUEUE';
export const PLAYBACK_SET_REWIND_STATE = 'PLAYBACK_SET_REWIND_STATE';
export const playbackToggleOneway = payload => action(PLAYBACK_TOGGLE_ONEWAY);
export const playbackReset = () => action(PLAYBACK_RESET);
export const playbackRewind = payload => action(PLAYBACK_REWIND);
export const playbackPause = payload => action(PLAYBACK_PAUSE);
export const playbackResume = payload => action(PLAYBACK_RESUME);
export const playbackHalt = payload => action(PLAYBACK_HALT);
export const playbackEnqueue = payload =>
  action(PLAYBACK_TRACK_ENQUEUE, payload);
export const playbackRewindState = payload =>
  action(PLAYBACK_SET_REWIND_STATE, payload);

export const LOG = 'LOG';
export const LOG_RESET = 'LOG_RESET';
export const logEvent = payload => action(LOG, payload);
export const logReset = payload => action(LOG_RESET, payload);

export const DEBUG_LOG = 'DEBUG_LOG';
export const DEBUG_LOG_RESET = 'DEBUG_LOG_RESET';
export const DEBUG_LOG_EXIT = 'DEBUG_LOG_EXIT';
export const debugLogAppExit = () => action(DEBUG_LOG_EXIT);
export const debugLog = payload => action(DEBUG_LOG, payload);
export const debugLogReset = () => action(DEBUG_LOG_RESET);

export const APP_THEME_AUDIO_ENABLETOGGLE = 'APP_THEME_AUDIO_ENABLETOGGLE';
export const APP_THEME_CLIP_PLAY = 'APP_THEME_CLIP_PLAY';
export const APP_THEME_CLIP_STOP = 'APP_THEME_CLIP_STOP';
export const APP_THEME_CLIP_PAUSE = 'APP_THEME_CLIP_PAUSE';
export const APP_THEME_CLIP_UNPAUSE = 'APP_THEME_CLIP_UNPAUSE';
export const APP_THEME_CLIP_DESTROY = 'APP_THEME_CLIP_DESTROY';

export const APP_THEME_CLIP_PAUSE_ALL = 'APP_THEME_CLIP_PAUSE_ALL';
export const APP_THEME_CLIP_UNPAUSE_ALL = 'APP_THEME_CLIP_UNPAUSE_ALL';

export const appToggleThemeAudio = () => action(APP_THEME_AUDIO_ENABLETOGGLE);
export const appThemeClipPlay = payload => action(APP_THEME_CLIP_PLAY, payload);
export const appThemeClipStop = payload => action(APP_THEME_CLIP_STOP, payload);
export const appThemeClipDestroy = payload =>
  action(APP_THEME_CLIP_DESTROY, payload);

export const appThemePauseAll = payload =>
  action(APP_THEME_CLIP_PAUSE_ALL, payload);

export const appThemeRestart = payload =>
  action(APP_THEME_CLIP_UNPAUSE_ALL, payload);
