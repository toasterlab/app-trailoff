import initialState from './init/log';
import * as actions from 'state/actions';
import LOG_CONST from 'logConstants.js';

export default function app(state = initialState, action) {
  switch (action.type) {
    case actions.LOG: {
      let log = state[action.payload.uuid];
      if (!log) log = [];
      log.push({
        date: Date.now(),
        event: action.payload.event,
        id: action.payload.id,
        sessionId: action.payload.sessionId
      });
      const newState = {...state, [action.payload.uuid]: log};
      return withAnalytics(newState);
    }
    case actions.LOG_RESET: {
      return withAnalytics([]);
    }
    default:
      return state;
  }
}

function withAnalytics(newState) {
  let analytics = {};
  Object.keys(newState)
    .filter(key => key !== 'analytics')
    .forEach(key => {
      newState[key]
        .filter(item => item.event === LOG_CONST.TRIGGER_LOCATION)
        .forEach(item => {
          if (!analytics[key]) analytics[key] = {};
          if (!analytics[key][item.id]) analytics[key][item.id] = {};
          if (!analytics[key][item.id][item.sessionId])
            analytics[key][item.id][item.sessionId] = 0;
          analytics[key][item.id][item.sessionId]++;
        });
      analytics[key] = {
        ...analytics[key]
      };
    });
  return {...newState, analytics};
}
