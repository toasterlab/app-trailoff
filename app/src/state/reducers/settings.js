import initialState from './init/settings';
import * as actions from 'state/actions';

export default function app(state = initialState, action) {
  switch (action.type) {
    case actions.APP_DEBUG_MODE:
      return { ...state, app: { ...state.app, debugMode: !state.debugMode } };
    case actions.INCREMENT_LAUNCH_COUNT:
      return {
        ...state,
        app: {
          ...state.app,
          isInitialLaunch: state.app.launchCount === 0,
          launchCount: state.app.launchCount + 1
        }
      };
    case actions.APP_THEME_AUDIO_ENABLETOGGLE:
      return {
        ...state,
        app: { ...state.app, themeAudioEnabled: !state.app.themeAudioEnabled }
      };

    case actions.STORY_GET_MANIFEST__R:
      return {
        ...state,
        app: {
          ...state.app,
          manifest: {
            updatedAt: Date.now(),
            latest: action.payload
          }
        }
      };

    case actions.STORY_GET_MANIFEST__R:
      let newList = [...state.stories];
      action.payload.forEach(item => {
        const idx = state.stories.indexOf(
          state.stories.find(el => el.uuid === item.uuid)
        );
        if (idx > 0) {
          newList[idx] = {
            ...newList[idx],
            status: {
              ...newList[idx].status,
              pkg: {
                ...newList[idx].status.pkg,
                needsUpdate: false,
                current: { date: item.package_date, size: item.archiveSize }
              }
            }
          };
        }
      });
      return { ...state, stories: newList };

    case actions.PKG_REMOVE: {
      const idx = state.stories.indexOf(
        state.stories.find(el => el.uuid === action.payload)
      );
      const newState = [...state.stories];
      newState[idx] = {
        ...state.stories[idx],
        status: {
          ...state.stories[idx].status,
          pkg: {}
        }
      };
      return { ...state, stories: newState };
    }

    case actions.PKG_UNPACK_COMPLETE: {
      const idx = state.stories.indexOf(
        state.stories.find(el => el.uuid === action.payload.manifest.uuid)
      );
      const newState = [...state.stories];
      newState[idx] = {
        ...state.stories[idx],
        status: {
          ...state.stories[idx].status,
          pkg: {
            updateAvailable: false,
            ...action.payload,
            audiotourConfig: action.payload.audiotourConfig[0]
          }
        }
      };
      return { ...state, stories: newState };
    }

    case actions.DOWNLOAD_STATUS: {
      const newState = [...state.stories];
      newState[action.payload.idx] = {
        ...state.stories[action.payload.idx],
        download: {
          ...state.stories[action.payload.idx].download,
          ...action.payload.payload
        }
      };
      return { ...state, stories: newState };
    }

    case actions.UPDATE_STATUS: {
      const idx = action.payload.storyId;
      const payload = action.payload.payload
        ? action.payload.payload
        : action.payload;
      const newState = [...state.stories];
      newState[idx] = {
        ...state.stories[idx],
        status: {
          ...state.stories[idx].status,
          ...payload
        }
      };
      return { ...state, stories: newState };
    }

    case actions.STORY_UPDATE_WEATHER: {
      if (typeof action.payload.id !== 'number') {
        console.warn('STORY_UPDATE_WEATHER: Missing ID');
        return state;
      }
      let newState = [...state.stories];
      newState[action.payload.id] = {
        ...state.stories[action.payload.id],
        status: {
          ...state.stories[action.payload.id].status,
          weather: { ...action.payload.data, updatedAt: Date.now() }
        }
      };
      return { ...state, stories: newState };
    }

    default:
      return state;
  }
}
