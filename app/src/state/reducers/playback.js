import initialState from './init/playback';
import * as actions from 'state/actions';

export default function app(state = initialState, action) {
  switch (action.type) {
    case actions.PLAYBACK_TOGGLE_ONEWAY:
      return { ...state, oneWay: !state.oneWay };
    case actions.PLAYBACK_RESUME:
      return { ...state, isPaused: false };
    case actions.PLAYBACK_PAUSE:
      return { ...state, isPaused: true };
    case actions.PLAYBACK_SET_REWIND_STATE:
      return { ...state, rewindDisabled: action.payload };
    default:
      return state;
  }
}
