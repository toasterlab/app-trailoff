export default {
  isLocationFake: false,
  location: null,
  locationFake: {
    course: 193.75,
    speed: 3.42,
    longitude: 1,
    floor: 0,
    latitude: 2,
    accuracy: 10,
    timestamp: Date.now(),
    altitude: 0,
    altitudeAccuracy: -1,
  },
  weather: {
    lastUpdate: null,
  },
};
