import stories from 'assets/stories';
const addStatus = arr => {
  let storiesWithStatus = [];
  arr.forEach(story => {
    storiesWithStatus.push({
      uuid: story.uuid,
      id: story.id,
      download: {
        isDownloading: false,
        progress: 0,
        downloadTask: null
      },
      status: {
        weather: { lastUpdate: null },
        pkg: {}
      }
    });
  });
  return storiesWithStatus;
};

export default {
  app: {
    themeAudioEnabled: true,
    debugMode: false,
    launchCount: 0,
    isInitialLaunch: true,
    manifest: {}
  },
  stories: [...addStatus(stories)]
};
