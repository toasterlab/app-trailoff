import stories from 'assets/stories';
const addStatus = arr => {
  let storiesWithStatus = [];
  arr.forEach(story => {
    storiesWithStatus.push({
      ...story
    });
  });
  return storiesWithStatus;
};
export default [...addStatus(stories)];
