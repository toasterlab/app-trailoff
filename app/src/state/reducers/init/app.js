import perceivedTimeOfDay from "./perceivedTimeOfDay.js";
import perceivedWeather from "./perceivedWeather.js";
export default {
  currentStoryIdx: null,
  isLocationFake: false,
  themeAudioPaused: false,
  network: null,
  manifest: {},
  permissions: {},
  lastClick: { screen: "List", isInitial: true },
  settings: {
    perceivedTimeOfDay,
    perceivedWeather,
    assetServer: "https://assets.trailoff.org",
    appId: "fb979b51-a4d6-48b6-9307-1919bb2fb47c",
    packageDir: "packages",
    weather: {
      key: "f7f3c2cc9525dd3701a7b2a5c1de575f",
      staleAfterMinutes: 60,
    },
  },
  sessionProgressIndicator: 0,
  location: null,
  locationFake: {
    course: 193.75,
    speed: 3.42,
    longitude: 1,
    floor: 0,
    latitude: 2,
    accuracy: 10,
    timestamp: Date.now(),
    altitude: 0,
    altitudeAccuracy: -1,
  },
  dialog: {
    isVisible: false,
    title: "",
    description: "",
    cancelText: "Ok",
  },
  debug: {
    display: "",
  },
};
