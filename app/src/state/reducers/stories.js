import initialState from './init/stories';

export default function stories(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
