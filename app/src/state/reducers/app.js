import initialState from "./init/app";
import * as actions from "state/actions";
import { stat } from "react-native-fs";

export default function app(state = initialState, action) {
  switch (action.type) {
    case actions.UPDATE_CURRENT_STORYID:
      return { ...state, currentStoryIdx: action.payload };

    case actions.NETWORK_STATE_CHANGE:
      return { ...state, network: action.payload };

    case actions.APP_CLEAR_SESSIONID:
      return { ...state, sessionId: null, sessionProgress: 0 };

    case actions.APP_SET_SESSIONID:
      return { ...state, sessionId: action.payload };

    case actions.APP_SET_SESSION_PROGRESS:
      return {
        ...state,
        sessionProgress:
          action.payload > state.sessionProgress
            ? action.payload
            : state.sessionProgress,
      };

    case actions.APP_SET_SESSION_PROGRESS_INDICATOR:
      return {
        ...state,
        sessionProgressIndicator: action.payload,
      };

    case actions.APP_THEME_AUDIO_PAUSE:
      return {
        ...state,
        themeAudioPaused: true,
      };

    case actions.APP_THEME_AUDIO_UNPAUSE:
      return {
        ...state,
        themeAudioPaused: false,
      };

    case actions.PERMISSIONS_UPDATE__R:
      return {
        ...state,
        permissions: {
          ...state.permissions,
          [action.payload.permission]: action.payload,
        },
      };
    case actions.APP_FOOTER_BUTTON_CLICK:
      return {
        ...state,
        lastClick: {
          screen: action.payload,
          isInitial: state.lastClick.screen !== action.payload,
        },
      };
    case actions.APP_LOCATION_UPDATE: {
      const location = state.isLocationFake
        ? { ...state.locationFake }
        : { ...action.payload };
      return {
        ...state,
        location,
      };
    }
    case actions.APP_LOCATION_UPDATE_FAKE: {
      let locationFake = { ...state.locationFake, ...action.payload };
      locationFake.filtered = {
        lat: locationFake.latitude,
        lng: locationFake.longitude,
      };
      locationFake.unfiltered = {
        lat: locationFake.latitude,
        lng: locationFake.longitude,
      };
      return {
        ...state,
        locationFake,
      };
      return state;
    }

    case actions.APP_LOCATION_TOGGLE_FAKE: {
      const location = state.isLocationFake
        ? { ...state.location }
        : { ...state.locationFake };
      return {
        ...state,
        isLocationFake: !state.isLocationFake,
        ...location,
      };
    }

    case actions.APP_LOCATION_SET: {
      return {
        ...state,
        isLocationFake: action.payload,
      };
    }

    case actions.APP_DIALOG: {
      return {
        ...state,
        dialog: {
          ...action.payload,
        },
      };
    }
    case actions.DEBUG_LOG: {
      return {
        ...state,
        debug: {
          ...state.debug,
          log: `${action.payload}\n${state.debug.log}`,
        },
      };
    }
    case actions.DEBUG_LOG_RESET: {
      return {
        ...state,
        debug: {
          ...state.debug,
          log: "",
        },
      };
    }
    default:
      return state;
  }
}
