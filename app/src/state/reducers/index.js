import { combineReducers } from 'redux';
import app from './app';
import stories from './stories';
import user from './user';
import playback from './playback';
import log from './log';
import settings from './settings';

const rootReducer = combineReducers({
  app,
  stories,
  user,
  playback,
  log,
  settings
});

export default rootReducer;
