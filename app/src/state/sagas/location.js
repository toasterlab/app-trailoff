import { take, put, select, call } from 'redux-saga/effects';
import * as actions from 'state/actions';
import { eventChannel } from 'redux-saga';
import turfDistance from '@turf/distance';
import { point } from '@turf/helpers';
import { GPSKalmanFilter } from 'utility/kalman.js';
import mathUtil from 'utility/math.js';
import RNLocation from 'react-native-location';
RNLocation.configure({
  distanceFilter: 1.0
});

export function* watchForLocationChange() {
  const channel = yield call(_start);
  try {
    while (true) {
      let payload = yield take(channel);
      yield put({ type: actions.INCOMING_LOCATION_UPDATE, payload });
    }
  } finally {
    // No op
  }
}

export function* _start() {
  return eventChannel(emitter => {
    let unsubscribe;
    RNLocation.requestPermission({
      ios: 'whenInUse',
      android: {
        detail: 'fine'
      }
    }).then(granted => {
      if (granted) {
        unsubscribe = RNLocation.subscribeToLocationUpdates(state => {
          emitter(state[0]);
        });
      }
    });
    return () => unsubscribe();
  });
}

const SAMPLE_SIZE = 10;
const REJECTION_THRESHOLD_MAX = 20;
const KALMAN_Q = 3;
const kalmanFilter = new GPSKalmanFilter(KALMAN_Q);
let sample = [];
let sampleDistance = [];
let previousCoordinate = { lng: 0, lat: 0 };
export const filter = location => {
  let unfiltered = { lat: location.latitude, lng: location.longitude };

  // Thresholding
  let filtered = { ...unfiltered };
  let thresholdRejection = 0;
  const distance = turfDistance(
    point([filtered.lat, filtered.lng]),
    point([previousCoordinate.lat, previousCoordinate.lng]),
    { units: 'meters' }
  );

  if (sampleDistance.length > SAMPLE_SIZE) {
    sampleDistance.shift();
  }
  if (distance <= REJECTION_THRESHOLD_MAX) sampleDistance.push(distance);
  if (distance >= mathUtil.medianOf(sampleDistance)) {
    thresholdRejection = distance;
  }
  previousCoordinate = { ...filtered };

  // Kalman
  if (thresholdRejection <= 0) {
    if (sample.length > SAMPLE_SIZE) {
      sample.shift();
    }
    sample.push(filtered);

    let filteredSet = [];
    sample.forEach(coord => {
      filteredSet.push(
        kalmanFilter.process(
          coord.lat,
          coord.lng,
          location.accuracy,
          location.timestamp
        )
      );
    });

    const latMedian = mathUtil.medianOf(filteredSet.map(item => item[1]));
    const lngMedian = mathUtil.medianOf(filteredSet.map(item => item[0]));
    filtered = { lat: latMedian, lng: lngMedian };
  }
  return {
    ...location,
    unfiltered,
    filtered
  };
};
