import { channel } from 'redux-saga';
import { take, put, select } from 'redux-saga/effects';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';
import * as actions from 'state/actions';
import { unzip, subscribe } from 'react-native-zip-archive';
import AUDIOTOUR_CONST from 'audioTourConstants.js';
import { readableFileSize } from 'util';
const downloadPackageChannel = channel();
export function* watchDownloadPackageChannel() {
  while (true) {
    const action = yield take(downloadPackageChannel);
    yield put(action);
  }
}

export const download = function*(action) {
  const uuid = action.payload;
  const reduxState = yield select();
  const idx = reduxState.settings.stories.indexOf(
    reduxState.settings.stories.find(story => story.uuid === uuid)
  );
  const assetSettings = reduxState.app.settings;
  const downloadUrl = `${assetSettings.assetServer}/${assetSettings.appId}/${assetSettings.packageDir}/${uuid}.zip`;
  const destDir = `${RNFetchBlob.fs.dirs.DocumentDir}/package/${uuid}`;
  let freeSpaceOnDevice = -1;

  const failTimer = setTimeout(() => {
    onErr(
      {
        msg:
          'We are having trouble accessing that story right now, please check your internet connection and try again later.'
      },
      'Check Connection'
    );
  }, 5000);

  const onErr = (err, title) => {
    downloadPackageChannel.put({
      type: actions.PKG_DOWNLOAD_CANCEL,
      payload: uuid
    });
    if (err.msg !== 'canceled') {
      downloadPackageChannel.put({
        type: actions.APP_DIALOG,
        payload: {
          isVisible: true,
          confirmText: null,
          cancelText: 'Ok',
          title: title ? title : 'Unavailable',
          description: err.msg.includes('zip')
            ? 'The requested story is not available right now. Please try again later.'
            : err.msg
        }
      });
    }
  };

  const onDeviceFull = needSize => {
    cancel();
    clearTimeout(failTimer);
    onErr(
      {
        msg: `There is not enough free space on this device to download this story, please free up ${readableFileSize(
          needSize,
          true
        )} and try again.`
      },
      'Device Full'
    );
  };

  const downloadTask =
    reduxState.settings.stories[reduxState.app.currentStoryIdx].download
      .downloadTask;
  if (downloadTask?.cancel) {
    yield downloadTask.cancel((err, downloadTaskId) => {
      if (err) console.log(err);
    });
    yield downloadPackageChannel.put({
      type: actions.DOWNLOAD_STATUS,
      payload: {
        idx,
        payload: {
          isDownloading: false,
          progress: 0,
          downloadTask: null
        }
      }
    });
  }

  downloadPackageChannel.put({
    type: actions.DOWNLOAD_STATUS,
    payload: {
      idx,
      payload: { isDownloading: true, progress: 0, downloadTask: null }
    }
  });

  RNFS.getFSInfo().then(info => {
    let downloadTask = RNFetchBlob.config({
      fileCache: true,
      appendExt: 'zip'
    })
      .fetch('GET', downloadUrl)
      .progress({ interval: 200 }, (received, total) => {
        if (total >= info.freeSpace) onDeviceFull(total);
        setTimeout(() => {
          downloadPackageChannel.put({
            type: actions.DOWNLOAD_STATUS,
            payload: {
              idx,
              payload: {
                progress: Math.round((received / total) * 100),
                downloadTask
              }
            }
          });
          clearTimeout(failTimer);
        }, 250);
      });

    downloadTask
      .then(res => {
        clearTimeout(failTimer);
        subscribe(({ progress, filePath }) => {}); // Zip progress (No-op)
        unzip(res.path(), destDir, 'UTF-8')
          .then(path => {
            RNFS.readFile(`${destDir}/audioTourConfig.json`, 'utf8').then(
              data => {
                const audiotourConfig = JSON.parse(data);
                RNFS.readFile(`${destDir}/manifest.json`, 'utf8').then(data => {
                  const manifest = JSON.parse(data);
                  if (manifest && manifest.uuid === uuid) {
                    RNFS.readFile(`${destDir}/audioTour.json`, 'utf8').then(
                      data => {
                        const audiotourData = JSON.parse(data);
                        RNFS.readFile(`${destDir}/config.json`, 'utf8').then(
                          data => {
                            const config = JSON.parse(data);
                            RNFS.readFile(`${destDir}/media.json`, 'utf8').then(
                              data => {
                                const media = JSON.parse(data);
                                RNFS.readFile(
                                  `${destDir}/poi.json`,
                                  'utf8'
                                ).then(data => {
                                  const poi = parsePoi(
                                    config,
                                    JSON.parse(data)
                                  );
                                  RNFS.readFile(
                                    `${destDir}/proximity.json`,
                                    'utf8'
                                  ).then(data => {
                                    const proximity = JSON.parse(data);
                                    const audiotour = parseAudiotour(
                                      audiotourData,
                                      media
                                    );
                                    downloadPackageChannel.put({
                                      type: actions.PKG_UNPACK_COMPLETE,
                                      payload: {
                                        manifest,
                                        media,
                                        audiotour,
                                        audiotourConfig,
                                        config,
                                        proximity,
                                        poi
                                      }
                                    });
                                  });
                                });
                              }
                            );
                          }
                        );
                      }
                    );
                  } else {
                    onErr({
                      msg: 'Cannot validate download'
                    });
                  }
                });
                RNFS.unlink(res.path()).then(() => {
                  //No-op
                });
              }
            );
          })
          .catch(e => {
            onErr({ msg: e.message });
          });
      })
      .catch(e => {
        onErr({ msg: e.message });
      });
  });
};

export const parseAudiotour = (audiotourData, media) => {
  let location = {};
  let proximity = {};

  audiotourData.forEach(trigger => {
    if (
      trigger.trigger_type_id === AUDIOTOUR_CONST.TRIGGER.LOCATION ||
      trigger.trigger_type_id === AUDIOTOUR_CONST.TRIGGER.PROXIMITY
    ) {
      let cleanTrigger = {
        id: trigger.id,
        transitionType: trigger.transition_type_id,
        transitionOptions: trigger.transition_options,
        loop: trigger.does_loop,
        ducksOthers: trigger.does_duck,
        triggerOptions: trigger.trigger_type_options,
        oneShot: trigger.only_once,
        sessionSkip: trigger.session_skip,
        neverFollowing: trigger.never_after,
        onlyFollowing: trigger.only_after,
        delayFollowMs: trigger.follow_at,
        onlyWhenWeather: trigger.on_weather,
        onlyWhenTod: trigger.on_tod,
        onlyOnTiggerVisitCount: trigger.fires_on_count,
        onlyOnGlobalVisitCount: trigger.fires_on_count_global,
        sortorder: trigger.sortorder
      };

      if (cleanTrigger !== null) {
        cleanTrigger.media = AUDIOTOUR_CONST.SILENCE;
        cleanTrigger.media = media.find(item => item.id === trigger.media_id);
        if (trigger.trigger_type_id === AUDIOTOUR_CONST.TRIGGER.LOCATION) {
          if (!location[trigger.poi_id]) location[trigger.poi_id] = {};
          if (!location[trigger.poi_id][trigger.track_id])
            location[trigger.poi_id][trigger.track_id] = [];
          location[trigger.poi_id][trigger.track_id].push(cleanTrigger);
          location[trigger.poi_id][trigger.track_id].sort(
            (a, b) => a.sortorder - b.sortorder
          );
        } else {
          if (!proximity[trigger.proximity_id])
            proximity[trigger.proximity_id] = {};
          if (!proximity[trigger.proximity_id][trigger.track_id])
            proximity[trigger.proximity_id][trigger.track_id] = [];
          proximity[trigger.proximity_id][trigger.track_id].push(cleanTrigger);
        }
      }
    }
  });

  // If the item in the 0 position has a transition of NONE
  // then the rest of the playback list is vestigal data and should be removed
  // Annoying we can't reliably do it above (but we can't guarentee the order)
  let cleanLocation = {};
  Object.keys(location).forEach(poi =>
    Object.keys(location[poi]).forEach((track, idx) => {
      if (
        location[poi][idx] &&
        location[poi][idx][0].transitionType !== AUDIOTOUR_CONST.TRANSITION.NONE
      ) {
        if (!cleanLocation[poi]) cleanLocation[poi] = {};
        if (!cleanLocation[poi][idx]) cleanLocation[poi][idx] = [];
        cleanLocation[poi][idx] = location[poi][idx];
      }
    })
  );
  return { location: cleanLocation, proximity };
};

export const parsePoi = (config, poi) => {
  const parsedPoi = {};
  poi.forEach(item => {
    let data = parsedPoi[item.poi_id]?.data ? parsedPoi[item.poi_id].data : [];
    data.push({ field: item.field, value: item.field_value });
    parsedPoi[item.poi_id] = {
      representation_id: item.representation_id,
      representation_config: item.representation_config,
      name: item.poi_name,
      date: item.poi_date,
      coordinate: item.poi_coordinate,
      sortorder: item.poi_sortorder,
      data
    };
  });
  let array = [];
  Object.keys(parsedPoi).forEach(id =>
    array.push({ id: parseInt(id, 10), ...parsedPoi[id] })
  );
  array.sort((a, b) => a.sortorder - b.sortorder);
  return array;
};

export const cancel = function*(action) {
  const reduxState = yield select();
  const idx = reduxState.settings.stories.indexOf(
    reduxState.settings.stories.find(story => story.uuid === action.payload)
  );
  const downloadTask =
    reduxState.settings.stories[reduxState.app.currentStoryIdx].download
      .downloadTask;
  if (downloadTask?.cancel) {
    yield downloadTask.cancel((err, downloadTaskId) => {
      if (err) console.log(err);
    });
    yield downloadPackageChannel.put({
      type: actions.DOWNLOAD_STATUS,
      payload: {
        idx,
        payload: {
          isDownloading: false,
          progress: 0,
          downloadTask: null
        }
      }
    });
  } else {
    yield downloadPackageChannel.put({
      type: actions.DOWNLOAD_STATUS,
      payload: {
        idx,
        payload: {
          isDownloading: false,
          progress: 0
        }
      }
    });
  }
};

export const remove = function*(action) {
  const packageDir = `${RNFetchBlob.fs.dirs.DocumentDir}/package/${action.payload}`;
  RNFS.unlink(packageDir)
    .then(() => {
      //No-op
    })
    .catch(err => {
      console.log(err);
    });
};
