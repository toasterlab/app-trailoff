import { put, select } from 'redux-saga/effects';
import * as actions from 'state/actions';

export function* getManifest() {
  const reduxState = yield select();
  const manifestUrl = `${reduxState.app.settings.assetServer}/${reduxState.app.settings.appId}/${reduxState.app.settings.packageDir}/manifest.json`;
  try {
    const response = yield fetch(manifestUrl, {
      method: 'GET',
      headers: { pragma: 'no-cache', 'cache-control': 'no-cache' }
    });
    let response_json = yield response.json();
    yield put({
      type: actions.STORY_GET_MANIFEST__R,
      payload: response_json
    });
  } catch (e) {
    console.log(e);
  }
}
