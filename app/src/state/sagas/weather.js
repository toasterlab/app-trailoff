import { take, put, select, call } from 'redux-saga/effects';
import * as actions from 'state/actions';

const weatherUpdate = function*(story) {
  const reduxState = yield select();
  const weatherSettings = reduxState.app.settings.weather;
  const storyStatus = story.status;
  const bellwetherPoi = storyStatus.pkg?.poi?.find(
    poi => poi.id == storyStatus.pkg.audiotourConfig.bellwether_poi
  );
  if (!bellwetherPoi) return;
  const url = `https://api.openweathermap.org/data/2.5/onecall?lat=${bellwetherPoi.coordinate.lat}&lon=${bellwetherPoi.coordinate.lng}&appid=${weatherSettings.key}`;
  const lastUpdateMinutesAgo =
    (Date.now() - storyStatus.weather?.updatedAt
      ? storyStatus.weather.updatedAt
      : 0) /
    1000 /
    60;
  if (
    lastUpdateMinutesAgo < weatherSettings.staleAfterMinutes ||
    !storyStatus.weather?.updatedAt
  ) {
    try {
      let response = yield fetch(url, { method: 'GET' });
      let response_json = yield response.json();
      if (response.ok) {
        yield put({
          type: actions.STORY_UPDATE_WEATHER,
          payload: { id: story.id, data: response_json }
        });
      } else {
        console.log(`WEATHER: Update error ${url}`);
      }
    } catch (e) {
      console.log(`WEATHER: Update error ${url}`);
      console.log(e);
    }
  }
};

export const updateCurrent = function*() {
  const reduxState = yield select();
  if (!reduxState.app.currentStoryIdx) return;
  const story = reduxState.settings.stories[reduxState.app.currentStoryIdx];
  if (!story) return;
  yield weatherUpdate(story);
};

export const updateAll = function*() {
  const reduxState = yield select();
  if (reduxState.settings.stories?.length > 0)
    for (let i = 0; i < reduxState.settings.stories.length; i++) {
      const story = reduxState.settings.stories[i];
      yield weatherUpdate(story);
    }
};

const kelvinToFarenheit = kelvin => {
  return kelvin * (9 / 5) - 459.67;
};

const kelvinToCelsius = kelvin => {
  return kelvin - 273.15;
};
