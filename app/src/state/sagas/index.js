import * as api from './api.js';
import { takeLatest, all, fork, put } from 'redux-saga/effects';
import * as actions from 'state/actions';
import * as stories from 'state/sagas/stories';
import * as app from './app.js';
import * as permissions from './permissions.js';
import * as status from './status.js';
import * as weather from './weather.js';
import * as pkg from './pkg.js';
import * as playback from './playback.js';
import * as location from './location.js';

let previousTransition;
export default function* rootSaga() {
  yield fork(() => api.watchForNetworkChange());
  yield fork(() => permissions.checkPermissions());

  yield all([
    pkg.watchDownloadPackageChannel(),
    app.watchBridge(),
    playback.watchBridge(),
    playback.watchStatusQueue(),

    takeLatest(actions.APP_ENABLE_LOCATION_WATCH, app.enableLocationWatch),
    takeLatest(actions.PERMISSIONS_UPDATE, permissions.checkPermissions),
    takeLatest(actions.PERMISSIONS_REQUEST, permissions.requestPermissions),
    takeLatest(actions.APP_ERROR, app.error),
    takeLatest(actions.APP_ERROR_NONFATAL, app.errorNonfatal),
    takeLatest(actions.STORY_GET_MANIFEST, stories.getManifest),

    takeLatest(actions.APP_STATE_TRANSITION, function*(action) {
      if (action.payload !== previousTransition) {
        if (action.payload === 'BACKGROUND') {
        } else if (action.payload === 'FOREGROUND') {
        }
        previousTransition = action.payload;
      }
    }),

    takeLatest(actions.APP_LAUNCH, function*(action) {
      yield fork(app.onLaunch),
        yield fork(weather.updateAll),
        yield fork(stories.getManifest);
    }),

    takeLatest(actions.SET_CURRENT_STORYID, function*(action) {
      yield put({
        type: actions.UPDATE_CURRENT_STORYID,
        payload: action.payload
      });
      yield put({
        type: actions.WEATHER_UPDATE
      });
      yield put({
        type: actions.STATUS_UPDATE
      });
    }),

    takeLatest(actions.APP_LOCATION_UPDATE_FAKE, function*(action) {
      yield put({
        type: actions.APP_LOCATION_UPDATE
      });
      yield put({
        type: actions.WEATHER_UPDATE
      });
      yield put({
        type: actions.STATUS_UPDATE
      });
    }),

    takeLatest(actions.INCOMING_LOCATION_UPDATE, function*(action) {
      yield (payload = location.filter(action.payload));
      yield put({
        type: actions.APP_LOCATION_UPDATE,
        payload
      });
      yield put({
        type: actions.WEATHER_UPDATE
      });
      yield put({
        type: actions.STATUS_UPDATE
      });
    }),

    takeLatest(actions.WEATHER_UPDATE, weather.updateCurrent),
    takeLatest(actions.WEATHER_UPDATE_ALL, weather.updateAll),
    takeLatest(actions.STATUS_UPDATE, status.updateCurrent),
    takeLatest(actions.STATUS_UPDATE_ALL, status.updateAll),

    takeLatest(actions.PKG_DOWNLOAD, pkg.download),
    takeLatest(actions.PKG_DOWNLOAD_CANCEL, pkg.cancel),
    takeLatest(actions.PKG_REMOVE, pkg.remove),

    takeLatest(actions.DEBUG_LOG_EXIT, playback.logExit),
    takeLatest(actions.PLAYBACK_REWIND, playback.rewind),
    takeLatest(actions.PLAYBACK_PAUSE, playback.pause),
    takeLatest(actions.PLAYBACK_RESUME, playback.resume),
    takeLatest(actions.PLAYBACK_GO, playback.playQueue),
    takeLatest(actions.PLAYBACK_TRACK_ENQUEUE, playback.trackEnqueue),
    takeLatest(actions.PLAYBACK_HALT, playback.halt),
    takeLatest(actions.PLAYBACK_RESET, playback.resetSession),

    takeLatest(
      actions.APP_THEME_AUDIO_ENABLETOGGLE,
      app.themeAudioToggleEnable
    ),
    takeLatest(actions.APP_THEME_CLIP_PLAY, app.themeClipPlay),
    takeLatest(actions.APP_THEME_CLIP_STOP, app.themeClipStop),
    takeLatest(actions.APP_THEME_CLIP_PAUSE, app.themeClipPause),
    takeLatest(actions.APP_THEME_CLIP_UNPAUSE, app.themeClipUnPause),
    takeLatest(actions.APP_THEME_CLIP_DESTROY, app.themeClipDestroy),
    takeLatest(actions.APP_THEME_CLIP_PAUSE_ALL, app.themeClipPauseAll),
    takeLatest(actions.APP_THEME_CLIP_UNPAUSE_ALL, app.themeClipUnPauseAll)
  ]);
}
