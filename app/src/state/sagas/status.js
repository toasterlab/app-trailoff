import { put, select } from 'redux-saga/effects';
import * as actions from 'state/actions';
import sun from 'utility/sun';
import turfDistance from '@turf/distance';
import turfBearing from '@turf/bearing';
import { point } from '@turf/helpers';
import nearestPointOnLine from '@turf/nearest-point-on-line';
import AUDIOTOUR_CONST from 'audioTourConstants.js';

export const updateCurrent = function*() {
  const reduxState = yield select();
  if (reduxState.app.currentStoryIdx === null) return;
  const story = reduxState.settings.stories[reduxState.app.currentStoryIdx];
  if (!story) return;
  yield doUpdate(story);
};

export const updateAll = function*() {
  const reduxState = yield select();
  if (reduxState.settings.stories?.length > 0)
    for (let i = 0; i < reduxState.settings.stories.length; i++) {
      const story = reduxState.settings.stories[i];
      yield doUpdate(story);
    }
};

const doUpdate = function*(story) {
  const reduxState = yield select();
  if (!reduxState.app.location?.filtered) return;
  let currentLocation = reduxState.app.location.filtered;
  const snapThresholdMeters = 20;
  const proxTriggers = [];
  const bellwetherPoi = story.status.pkg?.poi?.find(
    poi => poi.id == story.status.pkg.audiotourConfig.bellwether_poi
  );
  const newPrevious = {
    ...reduxState.settings.stories[story.id].status
  };
  delete newPrevious.previous;

  if (story.status.pkg.audiotourConfig?.ideal_path) {
    const unSnapped = point([currentLocation.lng, currentLocation.lat]);
    const snapped = nearestPointOnLine(
      story.status.pkg.audiotourConfig.ideal_path,
      unSnapped,
      {
        units: 'kilometers'
      }
    );
    if (snapped.properties.dist * 1000 <= snapThresholdMeters) {
      currentLocation = {
        lng: snapped.geometry.coordinates[0],
        lat: snapped.geometry.coordinates[1]
      };
    }
    if (story.status.pkg?.proximity && story.status.previousLocation) {
      const unSnappedPrev = point([
        story.status.previousLocation.lng,
        story.status.previousLocation.lat
      ]);
      const snappedPrev = nearestPointOnLine(
        story.status.pkg.audiotourConfig.ideal_path,
        unSnappedPrev,
        {
          units: 'kilometers'
        }
      );
      const currentDistance = snapped.properties.dist * 1000;
      const prevDistance = snappedPrev.properties.dist * 1000;

      story.status.pkg.proximity.forEach(prox => {
        const dist = prox.options.distance;
        if (currentDistance > dist && prevDistance < dist) {
          proxTriggers.push({
            id: prox.id,
            direction: AUDIOTOUR_CONST.DIRECTION.OUTGOING,
            trigger: prox
          });
        } else if (currentDistance < dist && prevDistance > dist) {
          proxTriggers.push({
            id: prox.id,
            direction: AUDIOTOUR_CONST.DIRECTION.INCOMING,
            trigger: prox
          });
        }
      });
    }
  }

  const pTod = bellwetherPoi
    ? sun.info({
        date: Date.now(),
        coordinate: bellwetherPoi.coordinate
      })
    : null;

  const pWeather = story.status.weather?.current
    ? perceivedWeather(
        reduxState.app.settings.perceivedWeather,
        story.status.weather.current
      )
    : null;

  let payload = {
    previousLocation: currentLocation,
    proxTriggers,
    storyId: story.id,
    uuid: story.uuid,
    lastUpdate: Date.now(),
    distanceFromStart: 0,
    pTod,
    pWeather,
    poi: story.status.pkg?.poi
  };

  if (story.status.pkg?.poi) {
    const poiByDistance = sortPoiByDistanceFrom(
      story.status.pkg.poi,
      currentLocation
    );
    const nearest = poiByDistance[0];
    const start = poiByDistance.find(loc => loc.poi.sortorder == 0);

    const bearingToNearest = nearest
      ? turfBearing(
          point([currentLocation.lng, currentLocation.lat]),
          point([nearest.poi.coordinate.lng, nearest.poi.coordinate.lat])
        )
      : null;

    const bearingToStart = start
      ? turfBearing(
          point([currentLocation.lng, currentLocation.lat]),
          point([start.poi.coordinate.lng, start.poi.coordinate.lat])
        )
      : null;

    const insideStart = start
      ? nearest.inside && start.poi.id === nearest.poi.id
      : false;

    payload = {
      ...payload,
      distanceFromStart: start.distance,
      start,
      insideStart,
      insideNearest: nearest ? nearest.inside : null,
      bearingToStart,
      bearingToNearest,
      nearest,
      inside: nearest && nearest.inside ? nearest : null,
      poiByDistance,
      previous: newPrevious
    };
  }
  yield put({ type: actions.UPDATE_STATUS, payload });
};

const perceivedWeather = (possibilities, { weather, temp }) => {
  if (!weather) return null;
  let found;
  possibilities.forEach(possibility => {
    let matchedAll = false;
    if (!found) {
      if (possibility.parameters.tempRange.lower !== null)
        matchedAll = temp >= possibility.parameters.tempRange.lower;

      if (possibility.parameters.tempRange.upper !== null)
        matchedAll = temp <= possibility.parameters.tempRange.upper;

      if (possibility.parameters.conditionCodes.length > 0)
        matchedAll = possibility.parameters.conditionCodes.includes(
          weather[0].id
        );
      if (matchedAll) found = possibility;
    }
  });
  return found ? found : { name: 'UNKNOWN', desc: '?' };
};

const sortPoiByDistanceFrom = (poi, distanceFrom) => {
  let sorted = [];
  poi.forEach(location => {
    const distance = turfDistance(
      point([distanceFrom.lng, distanceFrom.lat]),
      point([location.coordinate.lng, location.coordinate.lat]),
      { units: 'kilometers' }
    );
    sorted.push({
      poi: location,
      id: location.id,
      inside: location.representation_config.radius
        ? distance <= parseFloat(location.representation_config.radius)
        : false,
      distance
    });
  });
  sorted.sort((a, b) =>
    a.distance > b.distance ? 1 : b.distance > a.distance ? -1 : 0
  );
  return sorted;
};
