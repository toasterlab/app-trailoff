import { select, put } from 'redux-saga/effects';
import { Platform } from 'react-native';
import { request, check, PERMISSIONS, RESULTS } from 'react-native-permissions';
import * as actions from 'state/actions';
const permission =
  Platform.OS === 'ios'
    ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
    : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;

export function* requestPermissions() {
  const current = yield request(permission);
  yield checkPermissions(current);
}

export function* checkPermissions(previous) {
  const reduxState = yield select();
  try {
    const result = yield check(permission);
    switch (result) {
      case RESULTS.UNAVAILABLE:
        yield put({
          type: actions.APP_ERROR_NONFATAL,
          payload: {
            permission,
            error:
              'This feature is not available (on this device / in this context)',
            source: 'checkPermissions'
          }
        });
        break;
      case RESULTS.DENIED:
        yield put({
          type: actions.PERMISSIONS_UPDATE__R,
          payload: {
            permission,
            result,
            desc: 'not been requested / is denied but requestable'
          }
        });
        break;
      case RESULTS.GRANTED:
        yield put({
          type: actions.PERMISSIONS_UPDATE__R,
          payload: {
            permission,
            result,
            desc: 'granted'
          }
        });
        if (reduxState.settings.app.isInitialLaunch)
          yield put({ type: actions.APP_ENABLE_LOCATION_WATCH });
        break;
      case RESULTS.BLOCKED:
        yield put({
          type: actions.PERMISSIONS_UPDATE__R,
          payload: {
            permission,
            result,
            desc: 'denied and not requestable anymore'
          }
        });
        break;
    }
  } catch (e) {
    yield put({
      type: actions.APP_ERROR_NONFATAL,
      payload: { error: e, source: 'checkPermissions' }
    });
  }
}
