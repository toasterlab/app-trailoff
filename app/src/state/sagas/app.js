import { channel } from 'redux-saga';
import { select, put, take, fork } from 'redux-saga/effects';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';
import * as actions from 'state/actions';
import RNLocation from 'react-native-location';
import { watchForLocationChange } from './location.js';
import VersionNumber from 'react-native-version-number';
import Sound from 'react-native-sound';
import AUDIOTOUR_CONST from 'audioTourConstants.js';
Sound.setCategory('Playback');
const { fs } = RNFetchBlob;
const bridge = channel();
export function* watchBridge() {
  while (true) {
    const action = yield take(bridge);
    yield put(action);
  }
}
const themeClips = {};
RNLocation.configure({
  distanceFilter: 1.0
});

export const enableLocationWatch = function*(action) {
  yield fork(() => watchForLocationChange());
};

export const onLaunch = function*(action) {
  console.log(
    `${VersionNumber.bundleIdentifier}: ${VersionNumber.buildVersion}`
  );
  yield put({
    type: actions.APP_THEME_CLIP_PLAY,
    payload: {
      sound: 'theme_apptheme_nkmaster_v01.mp3',
      followWithLoop: 'theme_postthemeloop_nkmaster_v01.mp3',
      isPartOfTheme: true
    }
  });
  yield put({ type: actions.APP_LOCATION_SET, payload: false });
  yield put({ type: actions.DEBUG_LOG_RESET });
  yield put({ type: actions.APP_CLEAR_SESSIONID });
  yield put({ type: actions.INCREMENT_LAUNCH_COUNT });
  const reduxState = yield select();
  if (!reduxState.settings.app.isInitialLaunch)
    yield put({ type: actions.APP_ENABLE_LOCATION_WATCH });
};

export const removeAssets = function*(action) {
  const assetDir = `${RNFetchBlob.fs.dirs.DocumentDir}/package/${action.payload.uuid}`;
  RNFS.unlink(assetDir)
    .then(() => {
      // Do nothing
    })
    .catch(err => {
      console.log(err);
    });
};

export const error = function*(action) {
  yield console.error(
    `😫 SAGA FATAL (${action.payload.source}): ${
      action.payload.error ? action.payload.error : 'UNKNOWN'
    }`
  );
};

export const errorNonfatal = function*(action) {
  yield console.warn(
    `😫 SAGA NONFATAL ERROR (${action.payload.source}): ${
      action.payload.error ? action.payload.error : 'UNKNOWN'
    }`
  );
};

export const themeAudioToggleEnable = function*(action) {
  const reduxState = yield select();
  if (reduxState.settings.app.themeAudioEnabled) {
    yield put({
      type: actions.APP_THEME_CLIP_PLAY,
      payload: {
        sound: 'theme_apptheme_nkmaster_v01.mp3',
        followWithLoop: 'theme_postthemeloop_nkmaster_v01.mp3',
        isPartOfTheme: true
      }
    });
  } else {
    yield put({
      type: actions.APP_THEME_CLIP_DESTROY,
      payload: {
        sound: 'theme_apptheme_nkmaster_v01.mp3'
      }
    });
  }
};

export const themeClipPlay = function*(action) {
  const reduxState = yield select();
  if (themeClips[action.payload.sound])
    themeClips[action.payload.sound].stop(() =>
      themeClips[action.payload.sound].release()
    );
  if (
    action.payload.isPartOfTheme &&
    !reduxState.settings.app.themeAudioEnabled
  )
    return;

  let filePath = action.payload.fullPath
    ? action.payload.fullPath
    : Platform.OS === 'ios'
    ? `${fs.dirs.MainBundleDir}/${action.payload.sound}`
    : `${action.payload.sound}`;

  if (action.payload.solo) {
    Object.keys(themeClips).forEach(clipName => {
      bridge.put({
        type: actions.APP_THEME_CLIP_PAUSE,
        payload: { sound: clipName }
      });
    });
  }
  themeClips[action.payload.sound] = new Sound(filePath, null, error => {
    if (error) {
      console.log(error);
    } else {
      if (themeClips[action.payload.sound]) {
        themeClips[action.payload.sound].payload = action.payload;
        const onComplete = () => {
          if (typeof action.payload.cb === 'function') action.payload.cb();
          if (action.payload.followWithLoop) {
            filePath =
              Platform.OS === 'ios'
                ? `${fs.dirs.MainBundleDir}/${action.payload.followWithLoop}`
                : `${action.payload.followWithLoop}`;
            themeClips[action.payload.sound] = new Sound(
              filePath,
              null,
              error => {
                if (error) {
                  console.log(error);
                } else {
                  themeClips[action.payload.sound].setNumberOfLoops(
                    AUDIOTOUR_CONST.FOREVER
                  );
                  themeClips[action.payload.sound].play();
                }
              }
            );
          } else {
            themeClips[action.payload.sound].release();
            delete themeClips[action.payload.sound];
          }
        };
        themeClips[action.payload.sound].onComplete = onComplete;
        themeClips[action.payload.sound].play(onComplete);
      }
    }
  });
};

export const themeClipStop = function*(action) {
  if (themeClips[action.payload.sound]) themeClips[action.payload.sound].stop();
  if (typeof themeClips[action.payload.sound].payload?.cb === 'function')
    themeClips[action.payload.sound].payload.cb();
  if (themeClips[action.payload.sound].payload?.solo) {
    Object.keys(themeClips).forEach(clipName => {
      if (clipName !== action.payload.sound)
        bridge.put({
          type: actions.APP_THEME_CLIP_UNPAUSE,
          payload: { sound: clipName }
        });
    });
  }
};

export const themeClipDestroy = function*(action) {
  if (themeClips[action.payload.sound]) {
    if (typeof themeClips[action.payload.sound].payload?.cb === 'function')
      themeClips[action.payload.sound].payload.cb();
    themeClips[action.payload.sound].stop(() => {
      themeClips[action.payload.sound].release();
      delete themeClips[action.payload.sound];
    });
  }
};

export const themeClipUnPause = function*(action) {
  if (themeClips[action.payload.sound]) {
    themeClips[action.payload.sound].setCurrentTime(
      themeClips[action.payload.sound].pausedAt
        ? themeClips[action.payload.sound].pausedAt
        : 0
    );

    themeClips[action.payload.sound].play(() => {
      if (typeof themeClips[action.payload.sound].onComplete === 'function') {
        themeClips[action.payload.sound].onComplete();
      }
    });
  }
};

export const themeClipPause = function*(action) {
  if (themeClips[action.payload.sound]) {
    themeClips[action.payload.sound].getCurrentTime(
      sec => (themeClips[action.payload.sound].pausedAt = sec)
    );
    themeClips[action.payload.sound].stop();
  }
};

export const themeClipPauseAll = function*(action) {
  yield put({
    type: actions.APP_THEME_CLIP_DESTROY,
    payload: {
      sound: 'theme_apptheme_nkmaster_v01.mp3'
    }
  });
};

export const themeClipUnPauseAll = function*(action) {
  yield put({
    type: actions.APP_THEME_CLIP_PLAY,
    payload: {
      sound: 'theme_apptheme_nkmaster_v01.mp3',
      followWithLoop: 'theme_postthemeloop_nkmaster_v01.mp3',
      isPartOfTheme: true
    }
  });
};
