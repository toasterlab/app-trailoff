/*
Rewind:
Rewind works by first checking if *all* currently playing audio tracks can be safely brought back REWIND_SECONDS, no timers are running, 
no fades are in progress, we are not paused, and then proceeding only if this is possible. 

Thus if a track is > REWIND_SECONDS or playing earlier than that, rewind does nothing. 
This is done to avoid messing up the timing of cues which are triggered by end-of-play callbacks
*/

import { channel } from "redux-saga";
import {
  select,
  put,
  take,
  actionChannel,
  call,
  delay,
  flush,
  race,
} from "redux-saga/effects";
import Sound from "react-native-sound";
import RNFetchBlob from "rn-fetch-blob";
import AUDIOTOUR_CONST from "audioTourConstants.js";
import { store } from "state/store";
import * as actions from "state/actions";
import LOG_CONST from "logConstants.js";
import "react-native-get-random-values";
import { v4 as uuidv4 } from "uuid";
import pausable from "utility/pausableTimers.js";
const { fs } = RNFetchBlob;
Sound.setCategory("Playback");
let previouslyInside;
let pauseButtonCheckTimer;

const debugLog = (msg) => {
  bridge.put({
    type: actions.DEBUG_LOG,
    payload: msg,
  });
};

const AudioMgr = {
  queue: [],
  player: [],
  previous: [],
  pause: () => {
    AudioMgr.player.forEach((player) => {
      player.sound.getCurrentTime((sec) => {
        player.pausedAt = sec;
        player.sound.stop(() => {
          player.delayTimer?.pause();
          player.fadeTimer?.pause();
          player.isPaused = true;
        });
      });
    });
    AudioMgr.previous.forEach((player) => {
      if (player) {
        player.getCurrentTime((sec) => {
          player.pausedAt = sec;
          player.stop(() => {
            player.isPaused = true;
          });
        });
      }
    });
  },
  resume: () => {
    AudioMgr.player.forEach((player) => {
      if (player.sound && player.pausedAt) {
        player.sound.setCurrentTime(player.pausedAt);
        player.fadeTimer?.resume();
        const remaining = player.delayTimer.remaining;
        player.delayTimer.cancel();
        if (player.pausedAt > 0) {
          player.delayTimer = pausable.setTimeout(() => {
            player.sound.play(() => {
              if (typeof player.onComplete === "function") {
                player.onComplete();
              }
            });
          }, remaining);
        }
        player.isPaused = false;
      }
    });

    AudioMgr.previous.forEach((player) => {
      if (player && player.pausedAt) {
        player.setCurrentTime(player.pausedAt);
      }
    });
  },
  isRewindPossible: (rewindDuration, cb) => {
    let isPossible = true;
    if (AudioMgr.player.length === 0) {
      console.log(AudioMgr);
      cb(false);
    } else {
      AudioMgr.player.forEach((player, idx) => {
        isPossible = isPossible && player.sound.getDuration() >= rewindDuration;

        if (player.delayTimer) {
          if (isPossible && player.sound.getDuration() <= rewindDuration) {
            //DEBUG: console.log('Blocked by delay timer');
            isPossible = false;
          }
        }

        player.sound.getCurrentTime((elapsed) => {
          isPossible = isPossible && elapsed > rewindDuration;
          if (idx + 1 == AudioMgr.player.length) {
            //DEBUG: console.log(`${isPossible}:${elapsed}`);
            cb(isPossible);
          }
        });
      });
    }
  },
  rewindIfPossible: (rewindDuration) => {
    AudioMgr.isRewindPossible(rewindDuration, (isPossible) => {
      if (isPossible) AudioMgr._rewind(rewindDuration);
    });
  },
  _rewind: (rewindBy) => {
    //DEBUG: console.log('rewinding');
    AudioMgr.player.forEach((player) => {
      player.sound.getCurrentTime((currentTime) => {
        const newTime = currentTime - rewindBy;
        player.sound.setCurrentTime(newTime > 0 ? newTime : 0);
        if (player.delayTimer) {
          const remaining = player.delayTimer.remaining;
          player.delayTimer.cancel();
          if (player.pausedAt > 0) {
            player.delayTimer = pausable.setTimeout(() => {
              player.sound.play(() => {
                if (typeof player.onComplete === "function") {
                  player.onComplete();
                }
              });
            }, remaining - rewindBy);
          }
        }
      });
    });
  },
};

const bridge = channel();
export function* watchBridge() {
  while (true) {
    const action = yield take(bridge);
    yield put(action);
  }
}

export function* watchStatusQueue() {
  const requestChan = yield actionChannel(actions.UPDATE_STATUS);
  while (true) {
    const { payload } = yield take(requestChan);
    const { task, cancel } = yield race({
      task: call(incomingStatusUpdate, payload),
      cancel: take(actions.PLAYBACK_RESET_QUEUE),
    });

    if (cancel !== undefined) {
      yield flush(requestChan);
      // Don't need these actions, do nothing.
    }
    yield delay(250);
  }
}

export function* incomingStatusUpdate(payload) {
  const reduxState = store.getState();
  const story = reduxState.settings.stories[payload.storyId];
  const sessionId = reduxState.app.sessionId;

  const insideSomePoi = story.status.inside && story.status.inside !== null;
  if (!story.status.pkg.audiotour || !sessionId) return;

  // Location supercedes Proximity
  if (insideSomePoi) {
    if (story.status.inside.id !== previouslyInside?.id) {
      previouslyInside = story.status.inside;
      yield put({
        type: actions.LOG,
        payload: {
          uuid: story.uuid,
          event: LOG_CONST.TRIGGER_LOCATION,
          id: story.status.inside.id,
          sessionId,
        },
      });

      // Update progress gate
      let progressGate = story.status.inside.poi.data.find(
        (data) => data.field === "Progress Gate"
      );
      if (progressGate) progressGate = parseInt(progressGate.value, 10);
      if (!isNaN(progressGate)) {
        yield put({
          type: actions.APP_SET_SESSION_PROGRESS,
          payload: progressGate,
        });
      }

      const locationTriggers =
        story.status.pkg.audiotour.location[story.status.inside.id];
      if (locationTriggers) {
        bridge.put({
          type: actions.DEBUG_LOG,
          payload: `**${story.status.inside.poi.name} (${JSON.stringify(
            story.status.inside.poi.coordinate
          )})**\n`,
        });
        bridge.put({
          type: actions.DEBUG_LOG,
          payload: JSON.stringify(reduxState.app.location.filtered),
        });

        for (
          let trackId = 0;
          trackId < AUDIOTOUR_CONST.MAX_TRACK_COUNT;
          trackId++
        ) {
          let audio = locationTriggers[trackId];
          if (audio?.length > 0) {
            yield call(trackEnqueue, {
              trackId,
              audio,
              locationId: story.status.inside.id,
              sessionId,
            });
          }
        }
      }
    }
  } else if (story.status.proxTriggers) {
    for (let idx = 0; idx < story.status.proxTriggers.length; idx++) {
      const trigger = story.status.proxTriggers[idx];

      bridge.put({
        type: actions.DEBUG_LOG,
        payload: `**PROXIMITY ${trigger.trigger.name}**\n`,
      });
      bridge.put({
        type: actions.DEBUG_LOG,
        payload: JSON.stringify(trigger),
      });

      if (trigger.trigger.options.direction === trigger.direction) {
        const proxTracks =
          reduxState.settings.stories[reduxState.app.currentStoryIdx].status.pkg
            .audiotour.proximity[trigger.trigger.id];
        for (
          let trackId = 0;
          trackId < AUDIOTOUR_CONST.MAX_TRACK_COUNT;
          trackId++
        ) {
          let audio = proxTracks[trackId];
          if (audio?.length > 0) {
            yield call(trackEnqueue, {
              trackId,
              audio,
              proximityId: parseInt(trigger.trigger.id, 10),
              sessionId,
            });
          }
        }
      }
    }
  }
}

export function* rewind() {
  AudioMgr.rewindIfPossible(AUDIOTOUR_CONST.REWIND_SECONDS);
}

export function* pause() {
  AudioMgr.pause();
}

export function* resume() {
  AudioMgr.resume();
}

export function* halt() {
  bridge.put({ type: actions.PLAYBACK_RESET_QUEUE });
  bridge.put({ type: actions.APP_CLEAR_SESSIONID });
  clearTimeout(pauseButtonCheckTimer);
  pauseButtonCheckTimer = null;
  AudioMgr.player.forEach((player) => {
    if (player) {
      player.delayTimer?.cancel();
      player.fadeTimer?.cancel();
      player.sound.stop(() => {
        player.sound.release();
      });
    }
    if (player.previousSound) {
      player.previousSound.stop(() => {
        player.previousSound.release();
      });
    }
  });
  AudioMgr.player = [];

  bridge.put({
    type: actions.DEBUG_LOG,
    payload: "**AUDIO HALT!**",
  });
}

export function* resetSession() {
  yield call(halt);
  yield put({ type: actions.DEBUG_LOG_RESET });
  yield put({ type: actions.APP_SET_SESSIONID, payload: uuidv4() });
  yield put({ type: actions.APP_SET_SESSION_PROGRESS, payload: null });
  yield put({
    type: actions.APP_SET_SESSION_PROGRESS_INDICATOR,
    payload: null,
  });
}

export function* trackEnqueue(payload) {
  const reduxState = yield select();
  const uuid = reduxState.settings.stories[reduxState.app.currentStoryIdx].uuid;
  const isPaused = reduxState.playback.isPaused;

  if (!pauseButtonCheckTimer) {
    pauseButtonCheckTimer = setInterval(() => {
      AudioMgr.isRewindPossible(
        AUDIOTOUR_CONST.REWIND_SECONDS,
        (isPossible) => {
          bridge.put({
            type: actions.PLAYBACK_SET_REWIND_STATE,
            payload: !isPossible,
          });
        }
      );
    }, 1000);
  }

  // Check for one-shot (if EVER fired)
  const status =
    reduxState.settings.stories[reduxState.app.currentStoryIdx].status;
  const isOneShot = payload.audio[0].oneShot;
  const skipSessionCheck = payload.audio[0].sessionSkip;
  const firedThisSession = reduxState.log.analytics[status.uuid]
    ? reduxState.log.analytics[status.uuid][payload.locationId] &&
      reduxState.log.analytics[status.uuid][payload.locationId][
        payload.sessionId
      ] > 1
    : false;
  const firedBefore = reduxState.log.analytics[status.uuid]
    ? typeof reduxState.log.analytics[status.uuid][payload.locationId] ===
      "object"
    : false;
  if (
    (!skipSessionCheck && firedThisSession && reduxState.playback.oneWay) ||
    (firedBefore && isOneShot)
  ) {
    bridge.put({
      type: actions.DEBUG_LOG,
      payload: `[T] ***SKIP:*** ${
        firedThisSession
          ? "Already fired this session"
          : "Oneshot (has fired before)"
      }`,
    });
    return;
  }

  // Playback according to transition
  const transition = payload.audio[0].transitionType;
  const transitionOptions = payload.audio[0].transitionOptions;
  AudioMgr.player[payload.trackId]?.delayTimer?.cancel();

  switch (transition) {
    case AUDIOTOUR_CONST.TRANSITION.FADE:
      debugLog(`[T] **FADE... ${JSON.stringify(transitionOptions)}**`);
      AudioMgr.queue[payload.trackId] = [...payload.audio];
      yield call(playQueue, {
        ...payload,
        uuid,
        fade: transitionOptions,
      });
      break;

    case AUDIOTOUR_CONST.TRANSITION.CUT:
      debugLog(`[T] **CUT...**`);
      AudioMgr.player[payload.trackId]?.sound?.stop(() => {
        AudioMgr.player[payload.trackId].sound.release();
      });
      AudioMgr.queue[payload.trackId] = [...payload.audio];
      yield call(playQueue, { ...payload, uuid, fade: false });
      break;

    case AUDIOTOUR_CONST.TRANSITION.DEVAMP:
      debugLog("[T] **DEVAMP...**");
      AudioMgr.queue[payload.trackId] = [
        ...(AudioMgr.queue[payload.trackId]
          ? AudioMgr.queue[payload.trackId]
          : []),
        ...payload.audio,
      ];
      if (!AudioMgr.player[payload.trackId].isPlaying) {
        yield call(playQueue, { ...payload, uuid, fade: false });
      } else {
        AudioMgr.player[payload.trackId].sound.setNumberOfLoops(0);
      }
      break;
    case AUDIOTOUR_CONST.TRANSITION.NONE:
    default:
      return;
  }
}

export function* playQueue(payload) {
  if (payload.payload) payload = payload.payload;
  const { uuid, trackId, locationId, sessionId } = payload;
  const reduxState = store.getState();
  const isPaused = reduxState.playback.isPaused;
  const status =
    reduxState.settings.stories[reduxState.app.currentStoryIdx].status;
  const analytics = reduxState.log.analytics;
  const media = AudioMgr.queue[trackId].shift();
  const useSilence = !media.media?.url;

  const playNext = (reason) => {
    if (reason) debugLog(reason);
    if (AudioMgr.queue[trackId].length > 0) {
      bridge.put({
        type: actions.PLAYBACK_GO,
        payload: { ...payload, uuid },
      });
    } else {
      AudioMgr.queue[trackId];
    }
  };

  // Never and Only Following
  const numberOfEncountersWith = (locationId) => {
    if (analytics[status.uuid]) {
      if (analytics[status.uuid][locationId]) {
        return analytics[status.uuid][locationId][sessionId]
          ? analytics[status.uuid][locationId][sessionId]
          : 0;
      }
    }
    return 0;
  };

  if (media.neverFollowing !== null) {
    if (numberOfEncountersWith(media.neverFollowing) > 0) {
      playNext(
        `[${trackId + 1}] ***SKIP:*** NEVER follow ${media.neverFollowing}`
      );
      return;
    }
  }

  if (media.onlyFollowing !== null) {
    if (numberOfEncountersWith(media.onlyFollowing) === 0) {
      playNext(
        `[${trackId + 1}] ***SKIP:*** ONLY follow ${media.onlyFollowing}`
      );
      return;
    }
  }

  // Visit counts
  const globalVisitCount = Object.keys(analytics).length;
  let triggerVisitCount = 0;
  const sessions = analytics[status.uuid]
    ? analytics[status.uuid][locationId]
    : null;
  if (sessions)
    Object.keys(sessions).forEach(
      (key) => (triggerVisitCount += sessions[key])
    );

  // Global visit count
  if (media.onlyOnGlobalVisitCount) {
    if (
      !media.onlyOnGlobalVisitCount
        .split(",")
        .includes(globalVisitCount.toString())
    ) {
      playNext(
        `[${trackId + 1}] ***SKIP:*** Fires on TRAIL VISIT ${
          media.onlyOnGlobalVisitCount
        } (is ${globalVisitCount})`
      );
      return;
    }
  }

  // POI visit count
  if (media.onlyOnTiggerVisitCount > 0) {
    if (
      !media.onlyOnTiggerVisitCount
        .split(",")
        .includes(triggerVisitCount.toString())
    ) {
      playNext(
        `[${trackId + 1}] ***SKIP:*** Fires on POI VISIT ${
          media.onlyOnTiggerVisitCount
        } (is ${triggerVisitCount})`
      );
      return;
    }
  }

  // Weather
  const onlyWhenWeather = parseInt(media.onlyWhenWeather, 10);
  if (status.pWeather && onlyWhenWeather >= 0) {
    if (onlyWhenWeather !== status.pWeather.id) {
      playNext(
        `[${trackId + 1}] ***SKIP:*** Only fires when the weather is ${
          reduxState.app.settings.perceivedWeather[onlyWhenWeather].name
        } and it is currently ${
          reduxState.app.settings.perceivedWeather[status.pWeather.id].name
        }`
      );
      return;
    }
  }

  // Time of Day
  const onlyWhenTod = parseInt(media.onlyWhenTod, 10);
  if (onlyWhenTod >= 0) {
    const currentTod = reduxState.app.settings.perceivedTimeOfDay.find(
      (tod) => tod.name === status.pTod.perceivedTimeOfDay()
    );
    if (onlyWhenTod !== currentTod.id) {
      playNext(
        `[${trackId + 1}] ***SKIP:*** Only fires in the ${
          reduxState.app.settings.perceivedTimeOfDay[onlyWhenTod].name
        } and it is currently ${currentTod ? currentTod.name : "UNKNOWN"}`
      );
      return;
    }
  }

  // Playsound callback
  const mediaPath = `${RNFetchBlob.fs.dirs.DocumentDir}/package/${uuid}`;
  const playSound = () => {
    const reduxState = store.getState();
    if (AudioMgr.previous[trackId]) {
      AudioMgr.previous[trackId].fadeTimer?.cancel();
      AudioMgr.previous[trackId]?.stop(() => {
        AudioMgr.previous[trackId]?.release();
      });
    }
    AudioMgr.previous[trackId] = AudioMgr.player[trackId].sound;

    let filePath = useSilence
      ? Platform.OS === "ios"
        ? `${fs.dirs.MainBundleDir}/silence.mp3`
        : `silence.mp3`
      : `${mediaPath}/${media.media?.url}`;

    // Setup clip playback
    AudioMgr.player[trackId].sound = new Sound(filePath, null, (err) => {
      AudioMgr.player[trackId].isPaused = false;
      AudioMgr.player[trackId].isPlaying = true;
      AudioMgr.player[trackId].media = media;
      AudioMgr.player[trackId].payload = payload;
      if (err) {
        console.warn(err);
      } else {
        // Loop
        if (media.loop) {
          debugLog(
            `[${trackId + 1}] **LOOPING${isPaused ? " (PAUSED)" : ""}:** ${
              media.media?.name ? media.media.name : "SILENCE"
            } (Delayed: ${media.delayFollowMs ? media.delayFollowMs : 0})`
          );
          AudioMgr.player[trackId].sound.setNumberOfLoops(
            AUDIOTOUR_CONST.FOREVER
          );
        } else {
          let delay =
            parseFloat(media.delayFollowMs) > 0
              ? parseFloat(media.delayFollowMs)
              : 0;
          debugLog(
            `[${trackId + 1}] **PLAYING${isPaused ? " (PAUSED)" : ""}:** ${
              media.media?.name ? media.media.name : "SILENCE"
            } ${delay > 0 ? `(was held ${delay})` : ""}`
          );
          store.dispatch(
            actions.updateProgressIndicator(reduxState.app.sessionProgress)
          );
        }

        // Duck others
        if (media.ducksOthers) {
          debugLog(
            `[${trackId + 1}] **DUCK${isPaused ? " (PAUSED-IGNORING)" : ""}**`
          );
          if (!isPaused)
            AudioMgr.player.forEach((player) => {
              player.sound.setVolume(0.25);
            });
          AudioMgr.player[trackId].sound.setVolume(1.0);
        }

        // Fade
        if (payload.fade && AudioMgr.previous[trackId]) {
          let duration = payload.fade.duration_ms
            ? parseFloat(payload.fade.duration_ms)
            : AUDIOTOUR_CONST.DEFAULT_FADE_TIME;
          const from = AudioMgr.previous[trackId]._filename.split("/")[
            AudioMgr.previous[trackId]._filename.split("/").length - 1
          ];
          const to = AudioMgr.player[trackId].sound._filename.split("/")[
            AudioMgr.player[trackId].sound._filename.split("/").length - 1
          ];
          //DEBUG: console.log(`Fading track ${trackId} from ${from} to ${to}`);
          AudioMgr.player[trackId].sound?.setVolume(0);
          AudioMgr.player[trackId].fadeTimer?.cancel();
          AudioMgr.player[trackId].fadeTimer = pausable.setInterval(() => {
            const volStep = 1 / AUDIOTOUR_CONST.FADE_STEPS;
            let vol1 = AudioMgr.previous[trackId]
              ? AudioMgr.previous[trackId].getVolume() - volStep
              : 0;
            let vol2 = AudioMgr.player[trackId].sound
              ? AudioMgr.player[trackId].sound.getVolume() + volStep
              : 0;

            vol1 = vol1 > 1 ? 1 : vol1 < 0 ? 0 : vol1;
            vol2 = vol2 > 1 ? 1 : vol2 < 0 ? 0 : vol2;

            AudioMgr.previous[trackId].setVolume(vol1);
            AudioMgr.player[trackId].sound.setVolume(vol2);
            //DEBUG: console.log(`${vol1}:${vol2}`);
            if (vol1 === 0 && vol2 === 1) {
              // Fade complete
              if (AudioMgr.previous[trackId]) {
                AudioMgr.previous[trackId].stop(() => {
                  AudioMgr.previous[trackId].release();
                  AudioMgr.previous[trackId] = null;
                });
              }
              AudioMgr.player[trackId].fadeTimer.cancel();
            }
          }, (duration / 1000 / AUDIOTOUR_CONST.FADE_STEPS) * 1000);
        } else {
          AudioMgr.player[trackId]?.sound.stop();
        }

        const onComplete = (success) => {
          if (success && AudioMgr.player[trackId]) {
            debugLog(
              `[${trackId + 1}] ***DONE:*** ${
                media.media?.name ? media.media.name : "SILENCE"
              }`
            );
            // Unduck
            if (media.ducksOthers) {
              debugLog(`[${trackId + 1}] **UN-DUCK**`);
              AudioMgr.player.forEach((player) => {
                player.sound.setVolume(1.0);
              });
            }
            AudioMgr.player[trackId].isPlaying = false;
            playNext();
          } else {
            playNext(
              media.media?.name
                ? `[${trackId + 1}] **ERROR:** FAILED TO PLAY ${
                    media.media?.name ? media.media.name : "SILENCE"
                  }`
                : ""
            );
          }
        };
        if (AudioMgr.player[trackId]) {
          AudioMgr.player[trackId].onComplete = onComplete;
          AudioMgr.player[trackId].sound.play(onComplete);
        }

        if (isPaused) {
          AudioMgr.player[trackId].pausedAt = 0;
          AudioMgr.player[trackId].sound.pause();
        }
      }
    });
  };

  // Fire the sound on delay (most have a delay of 0)
  let delay =
    parseFloat(media.delayFollowMs) > 0 ? parseFloat(media.delayFollowMs) : 0;
  if (delay > 0) {
    debugLog(
      `[${trackId + 1}] ***HOLDING:*** ${delay}ms (${
        media.media?.name ? media.media.name : "SILENCE"
      })`
    );
  }
  if (!AudioMgr.player[trackId]) AudioMgr.player[trackId] = {};
  AudioMgr.player[trackId].delayTimer?.cancel();
  AudioMgr.player[trackId].delayTimer = pausable.setTimeout(playSound, delay);
}

export function* logExit() {
  bridge.put({
    type: actions.DEBUG_LOG,
    payload: "**>>> USER QUIT APP! <<<**",
  });
}
