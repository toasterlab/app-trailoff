import {put, call, take} from 'redux-saga/effects';
import * as actions from 'state/actions';
import NetInfo from '@react-native-community/netinfo';
import {eventChannel} from 'redux-saga';

export function* watchForNetworkChange() {
  const channel = yield call(_start);
  try {
    while (true) {
      let payload = yield take(channel);
      yield put({type: actions.NETWORK_STATE_CHANGE, payload});
    }
  } finally {
    // No op
  }
}

export function* _start() {
  return eventChannel(emitter => {
    // Subscribe
    const unsubscribe = NetInfo.addEventListener(state => {
      emitter(state);
    });
    // Unsubscribe
    return () => unsubscribe();
  });
}
