import {StyleSheet} from 'react-native';
import fonts from 'styles/fonts';

export default StyleSheet.create({
  container: {backgroundColor: 'white'},
  headerRow: {flexDirection: 'row', alignItems: 'center'},
  title: {
    fontFamily: fonts.ostrich_sans_inline,
    fontSize: 40,
    textAlign: 'right',
  },
  trailname: {
    fontSize: 20,
    fontFamily: fonts.abel,
    textAlign: 'right',
    textTransform: 'uppercase',
  },
  author: {
    fontSize: 20,
    textTransform: 'uppercase',
    textAlign: 'right',
    fontFamily: fonts.abel,
  },
  header: {
    textTransform: 'uppercase',
    fontFamily: fonts.ostrich_sans_heavy,
  },
  summary: {
    fontFamily: fonts.hype,
  },
  summary_header: {
    textTransform: 'uppercase',
    fontFamily: fonts.abel,
    fontSize: 30,
  },
  hrule: {borderWidth: 0.5, borderColor: 'black', margin: 10},
  tags: {
    fontFamily: fonts.abel,
  },
  titleText: {textAlign: 'center'},
  bodyCopy: {margin: 20},
  paragraph: {textAlign: 'center', marginBottom: 20},
  footer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'white',
    height: 80,
  },
  footerItem: {
    margin: 10,
  },
});
