import fonts from 'styles/fonts';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  doc: { height: '100%', backgroundColor: 'white' },
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
  },
  headerRow: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10
  },
  title: {
    flexGrow: 1,
    zIndex: 0,
    elevation: 0,
    height: 50,
    marginLeft: 40,
    marginTop: 10,
    marginBottom: 0,
    padding: 0,
    borderWidth: 0
  },
  backButton: { width: 40, zIndex: 3, elevation: 4 },
  titleText: {
    marginLeft: -80,
    marginTop: 5,
    textAlign: 'center',
    fontWeight: '300',
    textTransform: 'uppercase',
    fontFamily: fonts.hype,
    fontSize: 35
  },
  header: {
    textAlign: 'center',
    fontWeight: '100',
    fontSize: 25,
    marginTop: 0,
    marginBottom: 13,
    textTransform: 'uppercase',
    fontFamily: fonts.hype_bold
  },
  list: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: 35
  },
  bodyCopy: { textAlign: 'center' },
  paragraph: {
    margin: 0,
    marginLeft: 30,
    marginRight: 30,
    marginBottom: 30,
    fontWeight: '400',
    fontFamily: fonts.avenir_next_condensed,
    fontSize: 20,
    lineHeight: 30,
    textAlign: 'center',
    justifyContent: 'center'
  },
  twoColContainer: {
    width: '80%',
    display: 'flex',
    flexDirection: 'column',
    borderWidth: 0,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  },
  twoColContainerInner: {
    display: 'flex',
    flexDirection: 'row',
    flexGrow: 1,
    marginBottom: 10,
    textAlign: 'center',
    justifyContent: 'center'
  },
  twoColCol: {
    flexGrow: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  },
  checkbox: {
    alignSelf: 'center'
  }
});
