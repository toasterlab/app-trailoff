// https://hybridheroes.de/blog/2019-03-29-react-native-fonts/

import {Platform} from 'react-native';
const isIos = Platform.OS === 'ios';
export default {
  ostrich_sans_light: isIos ? 'OstrichSans-Light' : 'ostrichsans_light',
  ostrich_sans_medium: isIos ? 'OstrichSans-Medium' : 'ostrichsans_medium',
  ostrich_sans_heavy: isIos ? 'OstrichSans-Heavy' : 'ostrichsans_heavy',
  ostrich_sans_inline: isIos
    ? 'Ostrich Sans Inline'
    : 'ostrichsansinline_regular',
  hype: isIos ? 'HYPE' : 'hype',
  hype_bold: isIos ? 'HYPE-Bold' : 'hype_bold',
  vtks: isIos ? 'VTKS INKED' : 'vtks',
  abel: isIos ? 'Abel-Regular' : 'abel_regular',
  avenir_next_condensed: isIos
    ? 'AvenirNextCondensed-Medium'
    : 'avenir_next_condensed',
  avenir_next_condensed_italic: isIos
    ? 'AvenirNextCondensed-MediumItalic'
    : 'avenir_next_condensed_italic',
};
