import fonts from 'styles/fonts';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  doc: {height: '100%', backgroundColor: 'white'},
  container: {
    backgroundColor: '#F5F3F3',
    justifyContent: 'center',
    alignItems: 'center',
    height: '85%',
    margin: 50,
  },
  headerRow: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
  },
  navigationDot: {},
  backButton: {width: 40, zIndex: 3, elevation: 4},
  titleText: {
    borderWidth: 0,
    width: '100%',
    marginTop: 5,
    textAlign: 'center',
    fontWeight: '300',
    textTransform: 'uppercase',
    fontFamily: fonts.hype,
    fontSize: 35,
  },
  bodyCopy: {
    textAlign: 'center',
    fontFamily: fonts.avenir_next_condensed,
    fontWeight: '300',
    fontSize: 20,
    margin: 10,
  },
  title: {
    textAlign: 'center',
    fontFamily: fonts.avenir_next_condensed,
    fontWeight: '300',
    fontSize: 50,
    margin: 10,
  },
  title2: {
    textAlign: 'center',
    fontFamily: fonts.avenir_next_condensed,
    fontWeight: '300',
    fontSize: 40,
    margin: 10,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    marginBottom: 30,
  },
  button: {
    marginTop: 20,
  },
});
