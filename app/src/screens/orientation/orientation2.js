import * as React from 'react';
import { View, SafeAreaView, ScrollView, Text } from 'react-native';
import NavigationDots from 'components/navigationDots';
import GestureRecognizer from 'react-native-swipe-gestures';
import { useNavigation } from '@react-navigation/native';
import styles from 'styles/orientation';
import { useSelector } from 'react-redux';
import { Platform } from 'react-native';
import { PERMISSIONS, RESULTS } from 'react-native-permissions';
const locationPermission =
  Platform.OS === 'ios'
    ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
    : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;

export default () => {
  const navigation = useNavigation();
  const permission = useSelector(
    state => state.app.permissions[locationPermission]
  );
  const isGranted = permission?.result === RESULTS.GRANTED;
  return (
    <View style={styles.doc}>
      <GestureRecognizer
        onSwipeLeft={() => navigation.push('Orientation3')}
        onSwipeRight={() => navigation.goBack()}
      >
        <SafeAreaView>
          {(isGranted && (
            <View style={styles.container}>
              <View>
                <Text style={styles.title2}>LOOK AT YOU GO! </Text>
              </View>
              <View>
                <Text style={styles.bodyCopy}>
                  To pick up what I'm putting down, click around and explore
                  each trail's story and features.{' '}
                </Text>
              </View>
              <View>
                <Text style={styles.bodyCopy}>
                  Hit LET'S GO and you'll see all the info you need to get going
                  and download a story.{' '}
                </Text>
              </View>
              <View>
                <Text style={styles.bodyCopy}>
                  Once you arrive at your trail’s start, hit BEGIN and I'll be
                  right there to guide you.
                </Text>
              </View>
              <View style={styles.footer}>
                <NavigationDots numberOfDots={5} selected={2} />
              </View>
            </View>
          )) || (
            <View style={styles.container}>
              <View>
                <Text style={styles.title2}>Uh-Oh! </Text>
              </View>
              <View>
                <Text style={styles.bodyCopy}>
                  This is a location-based app. It will not work without
                  permssion to know where you are. Please enable location
                  permissions via the settings menu!
                </Text>
              </View>
            </View>
          )}
        </SafeAreaView>
      </GestureRecognizer>
    </View>
  );
};
