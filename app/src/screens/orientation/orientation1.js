import * as React from 'react';
import {View, SafeAreaView, Text} from 'react-native';
import NavigationDots from 'components/navigationDots';
import GestureRecognizer from 'react-native-swipe-gestures';
import {useNavigation} from '@react-navigation/native';
import Button from 'components/button';
import styles from 'styles/orientation';
import {useDispatch, useSelector} from 'react-redux';
import {requestPermissions} from 'state/actions';
import {Platform} from 'react-native';
import {PERMISSIONS, RESULTS} from 'react-native-permissions';
const locationPermission =
  Platform.OS === 'ios'
    ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
    : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;

export default () => {
  const navigation = useNavigation();
  const permission = useSelector(
    state => state.app.permissions[locationPermission],
  );
  const isGranted = permission?.result === RESULTS.GRANTED;

  const dispatch = useDispatch();
  const onEnableLocation = () => {
    dispatch(requestPermissions());
    navigation.push('Orientation2');
  };
  return (
    <View style={styles.doc}>
      <GestureRecognizer
        onSwipeLeft={() => {
          if (isGranted) {
            navigation.push('Orientation2');
          }
        }}>
        <SafeAreaView>
          <View style={styles.container}>
            <View>
              <Text style={styles.bodyCopy}>Welcome to TRAILOFF!</Text>
            </View>
            <View>
              <Text style={styles.bodyCopy}>
                I'm the Ranger and my job is to help you on your journey. Ready to experience some amazing stories that will unfold as you walk on trails throughout the region?
              </Text>
            </View>
            {(!isGranted && (
              <>
                <View>
                  <Text style={styles.bodyCopy}>
                    First, you'll need to turn on Location Services so we can do
                    our thang.
                  </Text>
                </View>
                <View style={styles.button}>
                  <Button
                    height={50}
                    width={200}
                    fontSize={30}
                    onPress={onEnableLocation}
                    text={'Enable Location'}
                  />
                </View>
              </>
            )) || (
              <View style={styles.footer}>
                <NavigationDots numberOfDots={5} selected={1} />
              </View>
            )}
          </View>
        </SafeAreaView>
      </GestureRecognizer>
    </View>
  );
};
