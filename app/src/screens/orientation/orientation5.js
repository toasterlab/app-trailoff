import * as React from 'react';
import {View, SafeAreaView, Text} from 'react-native';
import Button from 'components/button';
import GestureRecognizer from 'react-native-swipe-gestures';
import {useNavigation} from '@react-navigation/native';
import styles from 'styles/orientation';
export default () => {
  const navigation = useNavigation();
  return (
    <View style={styles.doc}>
      <GestureRecognizer onSwipeRight={() => navigation.goBack()}>
        <SafeAreaView>
          <View style={styles.container}>
            <View>
              <Text style={styles.bodyCopy} t>
                That said, this journey is yours, suga, and you take it at your
                own risk! This Ranger recommends staying away from these trails
                after dark and prepping yourself beforehand with the right gear
                and maybe even a friend.
              </Text>
            </View>
            <View>
              <Text style={styles.bodyCopy}>
                Now go grab some headphones and I'll see you again soon!
              </Text>
            </View>
            <View style={styles.footer}>
              <Button
                height={50}
                width={200}
                fontSize={30}
                onPress={() => navigation.navigate('Normal')}
                text={'Got It'}
              />
            </View>
          </View>
        </SafeAreaView>
      </GestureRecognizer>
    </View>
  );
};
