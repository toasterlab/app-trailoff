import * as React from 'react';
import {View, SafeAreaView, ScrollView, Text} from 'react-native';
import NavigationDots from 'components/navigationDots';
import GestureRecognizer from 'react-native-swipe-gestures';
import {useNavigation} from '@react-navigation/native';
import styles from 'styles/orientation';
export default () => {
  const navigation = useNavigation();
  return (
    <View style={styles.doc}>
      <GestureRecognizer
        onSwipeLeft={() => navigation.push('Orientation5')}
        onSwipeRight={() => navigation.goBack()}>
        <SafeAreaView>
          <View style={styles.container}>
            <View>
              <Text style={styles.bodyCopy}>
                This wild ride is brought to you by Swim Pony in collaboration
                with the Pennsylvania Environmental Council and Toasterlab. Hit
                CONNECT to learn more about them and give feedback to make this
                app even better.
              </Text>
            </View>
            <View style={styles.footer}>
              <NavigationDots numberOfDots={5} selected={4} />
            </View>
          </View>
        </SafeAreaView>
      </GestureRecognizer>
    </View>
  );
};
