import * as React from 'react';
import {View, SafeAreaView, ScrollView, Text} from 'react-native';
import NavigationDots from 'components/navigationDots';
import GestureRecognizer from 'react-native-swipe-gestures';
import {useNavigation} from '@react-navigation/native';
import styles from 'styles/orientation';
export default () => {
  const navigation = useNavigation();
  return (
    <View style={styles.doc}>
      <GestureRecognizer
        onSwipeLeft={() => navigation.push('Orientation4')}
        onSwipeRight={() => navigation.goBack()}>
        <SafeAreaView>
          <View style={styles.container}>
            <View>
              <Text style={styles.bodyCopy}>
                And of course I need a moment to honor the original Rangers{' '}
              </Text>
            </View>

            <View>
              <Text style={styles.title2}>- THE LENNI-LENAPE -</Text>
            </View>
            <View>
              <Text style={styles.bodyCopy}>
                who shaped this land, thrived, and curated all this beauty and
                are STILL AT IT to this very day.
              </Text>
            </View>
            <View style={styles.footer}>
              <NavigationDots numberOfDots={5} selected={3} />
            </View>
          </View>
        </SafeAreaView>
      </GestureRecognizer>
    </View>
  );
};
