import React, { useState } from 'react';
import BurgerButton from 'components/burgerButton';
import { View, SafeAreaView, ScrollView, Text } from 'react-native';
import styles from 'styles/info';
import { useDispatch, useSelector } from 'react-redux';
import { toggleDebugMode, appDialog } from 'state/actions';
import Dialog from 'components/dialog';
export default () => {
  const [clickCount, setClickCount] = useState(0);
  const dispatch = useDispatch();
  const debugMode = useSelector(state => state.settings.app.debugMode);

  return (
    <View style={styles.doc}>
      <Dialog />
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.headerRow}>
            <View style={styles.backButton}>
              <BurgerButton />
            </View>
            <View style={styles.title}>
              <Text style={styles.titleText}>Credits</Text>
            </View>
          </View>
          <ScrollView style={styles.bodyCopy}>
            <Text style={styles.paragraph}>
              Lead Creator & Director {'//'} Adrienne Mackey
            </Text>
            <Text style={styles.paragraph}>
              Composition, Sound Design {'//'} Michael Kiley{' '}
            </Text>
            <Text
              style={styles.paragraph}
              onPress={() => {
                setClickCount(clickCount + 1);
                if (clickCount === 9) {
                  setClickCount(0);
                  dispatch(
                    appDialog({
                      isVisible: true,
                      title: 'DEBUG MODE 🐞',
                      description: `Debug mode ${debugMode ? 'OFF' : 'ON'}`,
                      cancelText: 'Ok'
                    })
                  );
                  dispatch(toggleDebugMode());
                }
              }}
            >
              App Production {'//'} Toasterlab (Ian Garrett, Justine Garrett,
              Andrew Sempere)
            </Text>
            <Text style={styles.paragraph}>
              PEC Program Management {'//'} Lizzie Hessek{' '}
            </Text>
            <Text style={styles.paragraph}>
              Graphic Design {'//'} Maria Shaplin
            </Text>
            <Text style={styles.paragraph}>
              Audio Mastering {'//'} Nick Krill
            </Text>
            <Text style={styles.paragraph}>
              Project Management {'//'} Nazlah Black, Eva Steinmetz,
              Jennifer Sung, Sara Wolff
            </Text>
            <Text style={styles.paragraph}>
              Ranger Script {'//'} TS Hawkins
            </Text>
            <Text style={styles.paragraph}>Ranger Voice {'//'} STARFIRE</Text>
            <Text style={styles.paragraph}>
              Photography {'//'} John C. Hawthorne
            </Text>
          </ScrollView>
        </View>
      </SafeAreaView>
    </View>
  );
};
