import * as React from 'react';
import BurgerButton from 'components/burgerButton';
import { View, SafeAreaView, ScrollView, Text, Image } from 'react-native';
import styles from 'styles/info';
import Link from 'components/link';
import VersionNumber from 'react-native-version-number';

export default () => {
  return (
    <View style={styles.doc}>
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.headerRow}>
            <View style={styles.backButton}>
              <BurgerButton />
            </View>
            <View style={styles.title}>
              <Text style={styles.titleText}>About</Text>
            </View>
          </View>
          <ScrollView style={styles.bodyCopy}>
            <Text style={styles.paragraph}>
              <Link url="http://trailoff.org" text="TrailOff" /> was created to
              explore inventive storytelling from diverse voices and promote the
              amazing outdoor spaces that surround us all.
            </Text>
            <Text style={styles.paragraph}>
              It was conceived by{' '}
              <Link url="https://swimpony.org/" text="Swim Pony" /> and created in
              collaboration with the{' '}
              <Link
                url="https://pecpa.org/"
                text="Pennsylvania Environmental Council"
              />{' '}
              & <Link url="http://toasterlab.com/" text="Toasterlab" />.
            </Text>
            <Text style={styles.paragraph}>
              To learn more about the land and water this app explores visit:{' '}
              <Link
                url="https://circuittrails.org/"
                text="The
          Circuit Trails"
              />{' '}
              & the{' '}
              <Link
                url="https://www.watershedalliance.org/"
                text="Alliance for Watershed Education"
              />
              .
            </Text>
            <View
              style={{
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <View style={styles.twoColContainer}>
                <View>
                  <Text style={{ opacity: 0.2 }}>
                    v.{VersionNumber.buildVersion}
                  </Text>
                </View>
                <View style={styles.twoColContainerInner}>
                  <View style={styles.twoColCol}>
                    <Image
                      source={require('assets/common/logo/swimpony.png')}
                    />
                  </View>
                  <View style={styles.twoColCol}>
                    <Image source={require('assets/common/logo/pec.png')} />
                  </View>
                </View>
                <View>
                  <Image
                    source={require('assets/common/logo/toasterlab.png')}
                  />
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    </View>
  );
};
