import BackButton from 'components/backButton.js';
import * as React from 'react';
import { View, SafeAreaView, ScrollView, Text, StyleSheet } from 'react-native';
import fonts from 'styles/fonts';
import Markdown from 'react-universal-markdown/native';
import { useSelector } from 'react-redux';
export default ({ route, navigation }) => {
  const story = useSelector(state => state.stories[route.params.story.id]);

  const CreditListing = props => {
    if (props.list.length === 0) {
      return null;
    } else {
      return (
        <View style={styles.container}>
          <Text style={styles.creditHeader}>{props.header}</Text>
          {props.list.map((credit, idx) => {
            return (
              <View key={`${props.header}_${idx}`}>
                <Text
                  style={styles.creditText}
                >{`${credit.name} // ${credit.credit}`}</Text>
              </View>
            );
          })}
        </View>
      );
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.pageHeaderBlock}>
        <View style={styles.aboveAll}>
          <BackButton icon="close" />
        </View>
        <View style={styles.pageTitle}>
          <Text style={styles.pageHeader}>Credits</Text>
        </View>
      </View>

      <View style={styles.container}>
        <ScrollView style={styles.innerContainer}>
          <View style={styles.container}>
            <Text style={styles.authorHeader}>Author</Text>
            <Text style={styles.creditText}>{story.author}</Text>
            <Text style={styles.creditText}>{story.coauthor}</Text>
            <CreditListing
              header={'Actors // Characters'}
              list={story.detail.credits.actors}
            />

            <CreditListing
              header={'Musicians'}
              list={story.detail.credits.musicians}
            />

            <Text style={styles.creditHeader}>Trail Partner</Text>
            <Markdown styles={{ Text: styles.creditText }}>
              {story.detail.credits.partner[0].name}
            </Markdown>
            <Text style={styles.creditHeader}>Special Thanks</Text>
            <Markdown styles={{ Text: styles.creditText }}>
              {story.detail.credits.thanks}
            </Markdown>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  },
  innerContainer: { flex: 1, width: 350 },
  image_headshot: { width: 240, height: 240 },
  authorHeader: {
    textTransform: 'uppercase',
    fontWeight: '300',
    fontFamily: fonts.avenir_next_condensed,
    textDecorationLine: 'underline',
    fontSize: 22,
    marginTop: 16,
    marginBottom: 10,
    padding: 0
  },
  pageHeaderBlock: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  pageTitle: {
    flex: 1
  },
  pageHeader: {
    marginTop: 8,
    marginLeft: -65,
    textTransform: 'uppercase',
    fontFamily: fonts.hype,
    fontSize: 30,
    textAlign: 'center'
  },
  creditHeader: {
    textTransform: 'uppercase',
    fontWeight: '300',
    fontFamily: fonts.avenir_next_condensed,
    textDecorationLine: 'underline',
    fontSize: 22,
    marginTop: 16,
    marginBottom: 10,
    padding: 0
  },
  creditText: {
    fontWeight: '400',
    fontFamily: fonts.avenir_next_condensed,
    fontSize: 17,
    lineHeight: 23,
    textAlign: 'center'
  },
  aboveAll: { zIndex: 3, elevation: 3 }
});
