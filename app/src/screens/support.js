import * as React from 'react';
import BurgerButton from 'components/burgerButton';
import { View, SafeAreaView, ScrollView, Text, Image } from 'react-native';
import defaultStyles from 'styles/info';

export default () => {
  const styles = {
    ...defaultStyles,
    logos: {
      margin: 5,
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center'
    },
    logoPair: {
      margin: 5,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center'
    },
    logo: { margin: 5 }
  };
  return (
    <View style={styles.doc}>
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.headerRow}>
            <View style={styles.backButton}>
              <BurgerButton />
            </View>
            <View style={styles.title}>
              <Text style={styles.titleText}>Support</Text>
            </View>
          </View>
          <ScrollView style={styles.bodyCopy}>
            <Text style={styles.paragraph}>
              TrailOff’s creation is possible thanks to:
            </Text>
            <View style={styles.logos}>
              <Image source={require('assets/common/logo/williampenn.png')} />
            </View>
            <View style={styles.logoPair}>
              <View style={styles.logo}>
                <Image source={require('assets/common/logo/wyncote.png')} />
              </View>
              <View style={styles.logo}>
                <Image source={require('assets/common/logo/barra.png')} />
              </View>
            </View>
            <View style={styles.logoPair}>
              <View style={styles.logo}>
                <Image
                  source={require('assets/common/logo/philcultural.png')}
                />
              </View>
              <View style={styles.logo}>
                <Image source={require('assets/common/logo/net.png')} />
              </View>
            </View>
            <View style={styles.logos}>
              <Image source={require('assets/common/logo/pca.png')} />
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    </View>
  );
};
