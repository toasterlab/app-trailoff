import React, { useState, useEffect } from 'react';
import BurgerButton from 'components/burgerButton';
import CheckBox from '@react-native-community/checkbox';
import { View, ScrollView, SafeAreaView, Text } from 'react-native';
import styles from 'styles/info';
import { useSelector, useDispatch } from 'react-redux';
import { useNetInfo } from '@react-native-community/netinfo';
import { appToggleThemeAudio } from 'state/actions';

const CheckBox2 = ({ value, title }) => {
  return <div>{title}</div>;
};

export default () => {
  const dispatch = useDispatch();
  const appState = useSelector(state => state.app);
  const themeAudioEnabled = useSelector(
    state => state.settings.app.themeAudioEnabled
  );
  const netInfo = useNetInfo();
  return (
    <View style={styles.doc}>
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.headerRow}>
            <BurgerButton />
            <View style={styles.title}>
              <Text style={styles.titleText}>Settings</Text>
            </View>
          </View>
          <ScrollView style={styles.bodyCopy}>
            <View
              style={{
                margin: 10,
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center'
              }}
            >
              <View style={{ marginRight: 10 }}>
                <CheckBox
                  value={themeAudioEnabled}
                  onValueChange={() => dispatch(appToggleThemeAudio())}
                  style={styles.checkbox}
                />
              </View>
              <View style={{ width: 150 }}>
                <Text>
                  Theme Audio {themeAudioEnabled ? 'Enabled' : 'Disabled'}
                </Text>
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    </View>
  );
};
