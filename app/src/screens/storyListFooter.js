import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import Button from 'components/button';
import { onFooterButtonClick } from 'state/actions';
import { appThemeClipPlay } from 'state/actions';

export default () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const onPress = screen => {
    /*
    dispatch(
      appThemeClipPlay({
        sound: 'theme_homepagebuttonsounds_nkmaster_v01.mp3',
        isPartOfTheme: true
      })
    );*/
    dispatch(onFooterButtonClick(screen));
    navigation.navigate(screen);
  };

  return (
    <View style={styles.footer}>
      <View style={styles.footerItem}>
        <Button
          height={30}
          width={100}
          fontSize={20}
          lineHeight={34}
          onPress={() => onPress('List')}
          text={'List View'}
        />
      </View>
      <View style={styles.footerItem}>
        <Button
          height={30}
          width={100}
          fontSize={20}
          lineHeight={34}
          onPress={() => onPress('Art')}
          text={'Art View'}
        />
      </View>
      <View style={styles.footerItem}>
        <Button
          height={30}
          width={100}
          fontSize={20}
          lineHeight={34}
          onPress={() => onPress('Map')}
          text={'Map View'}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  footer: {
    paddingBottom: Platform.OS === 'ios' ? 30 : 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'white',
    height: 80,
    borderWidth: 0
  },
  footerItem: {
    margin: 10
  }
});
