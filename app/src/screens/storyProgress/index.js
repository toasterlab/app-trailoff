import { useKeepAwake } from "expo-keep-awake";
import React, { useState, useEffect, useCallback } from "react";
import { AppState } from "react-native";
import {
  View,
  SafeAreaView,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
  Alert,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import BackButton from "components/backButton.js";
import LocationDebugger from "components/LocationDebugger.js";
import images from "assets/stories/images.js";
import styles from "./style.js";
import { pad } from "util";
import TornPaperHeader from "components/tornPaperHeader";
import {
  playbackPause,
  playbackResume,
  playbackRewind,
  debugLogAppExit,
  appThemeRestart,
  appThemeClipPlay,
  playbackReset,
} from "state/actions";
import "react-native-get-random-values";
let screen = Dimensions.get("window");

export default ({ route, navigation }) => {
  const dispatch = useDispatch();
  const story = useSelector((state) => state.stories[route.params.story.id]);
  const isPaused = useSelector((state) => state.playback.isPaused);
  const progressIndicator = useSelector(
    (state) => state.app.sessionProgressIndicator
  );
  const debugMode = useSelector((state) => state.settings.app.debugMode);
  const rewindDisabled = useSelector((state) => state.playback.rewindDisabled);
  const [isLocked, setIsLocked] = useState(false);

  // Blocks the screen from sleeping
  useKeepAwake();

  const pauseOnExit = useCallback(
    (state) => {
      if (state !== "active") {
        dispatch(debugLogAppExit());
        if (userInitiatedExit) {
          dispatch(playbackResume());
          dispatch(playbackReset());
        } else {
          dispatch(playbackPause());
        }
      }
    },
    [dispatch]
  );

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", (e) => {
      userInitiatedExit = false;
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    const unsubscribe = navigation.addListener("blur", (e) => {
      dispatch(appThemeRestart());
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    const unsubscribe = navigation.addListener("blur", pauseOnExit);
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    const unsubscribe = AppState.addEventListener("change", pauseOnExit);
    return unsubscribe;
  }, [pauseOnExit]);

  useEffect(
    () =>
      navigation.addListener("beforeRemove", (e) => {
        e.preventDefault();
        Alert.alert(
          "Quit trail?",
          "If you close this screen you will not be able to listen to the trail, do you really want to leave?",
          [
            { text: "Don't Quit", style: "cancel", onPress: () => {} },
            {
              text: "Quit Trail",
              style: "destructive",
              onPress: () => {
                userInitiatedExit = true;
                navigation.dispatch(e.data.action);
              },
            },
          ]
        );
      }),
    [navigation]
  );

  const progressStep = `progress_${pad(
    progressIndicator ? progressIndicator : 0,
    2
  )}`;
  const trailImageHeight =
    screen.height - 300 > images[story.uuid].info[progressStep].height
      ? images[story.uuid].info[progressStep].height
      : screen.height - 300;
  const trailImageWidth =
    screen.width > images[story.uuid].info[progressStep].width
      ? screen.width
      : images[story.uuid].info[progressStep].width;
  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.container}>
        <TornPaperHeader story={story} />
        <View style={{ top: -230 }}>
          <BackButton icon="close" />
        </View>
        <View
          style={{
            position: "absolute",
            marginTop: screen.height - trailImageHeight - 70,
          }}
        >
          <View style={styles.trailMap}>
            <Image
              style={{
                height: trailImageHeight,
                width: trailImageWidth,
                resizeMode: "contain",
              }}
              source={images[story.uuid][progressStep]}
            />
          </View>
          <View style={styles.buttons}>
            <TouchableOpacity
              disabled={isLocked}
              onPress={() => {
                if (!isPaused) {
                  dispatch(
                    appThemeClipPlay({
                      sound: "theme_softsimplesound_nkmaster_v01.mp3",
                    })
                  );
                  dispatch(playbackPause());
                } else {
                  dispatch(playbackResume());
                }
              }}
            >
              <View>
                <View
                  style={[
                    styles.playbackIcon,
                    {
                      backgroundColor: story.color,
                      opacity: isLocked ? 0.2 : 1,
                    },
                  ]}
                >
                  {(!isPaused && (
                    <Image
                      source={require("assets/common/icon/pause.png")}
                      style={styles.play_button}
                    />
                  )) || (
                    <Image
                      source={require("assets/common/icon/play.png")}
                      style={styles.play_button}
                    />
                  )}
                </View>
              </View>
            </TouchableOpacity>
            <View style={{ display: "flex", flexDirection: "row" }}>
              <View>
                <TouchableOpacity onPress={() => setIsLocked(!isLocked)}>
                  {(isLocked && (
                    <Image
                      source={require("assets/common/icon/lock.png")}
                      style={styles.lock_button}
                    />
                  )) || (
                    <Image
                      source={require("assets/common/icon/unlock.png")}
                      style={styles.lock_button}
                    />
                  )}
                </TouchableOpacity>
              </View>
              <View
                style={{
                  opacity: isPaused || rewindDisabled || isLocked ? 0.2 : 1,
                }}
              >
                <TouchableOpacity
                  disabled={isPaused || rewindDisabled || isLocked}
                  onPress={() => dispatch(playbackRewind())}
                >
                  <Image
                    source={require("assets/common/icon/skip15.png")}
                    style={styles.skip_button}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        {debugMode && <LocationDebugger story={story} />}
      </SafeAreaView>
    </View>
  );
};
