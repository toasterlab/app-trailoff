import React from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  SafeAreaView,
} from "react-native";

import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";
import StoryListFooter from "./storyListFooter";
import images from "assets/stories/images.js";

const ImageLink = (props) => {
  const i = images;
  const stories = useSelector((state) => state.stories);
  const navigation = useNavigation();
  const story = stories.find((item) => item.uuid === props.uuid);
  return (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate("Story", {
          story: { id: story.id, uuid: props.uuid },
        })
      }
    >
      <Image
        style={{
          ...props.style,
          height: images[props.uuid].info.art.height - props.adjustment,
          resizeMode: "contain",
        }}
        source={images[props.uuid].art}
      />
    </TouchableOpacity>
  );
};
let marginTop = 0;
const headerHeight = 170;
export default (props) => {
  const margin = 0;
  const footerHeight = 63;
  let height = Dimensions.get("window").height - (footerHeight + headerHeight);
  const width = Dimensions.get("window").width - 40;

  const col1 = [
    { uuid: "ed37fd2e-b3b8-4830-a5b0-01c173f44b25", style: { marginTop: 0 } },
    { uuid: "cc4ee685-088b-4899-9dc2-5c24f25fcc9d", style: { marginTop: 10 } },
    { uuid: "49386444-c777-4200-b0f7-02080e95999a", style: {} },
    { uuid: "0b9b1fd5-4cb7-4724-8fdc-c85121aaa30f", style: { marginLeft: 30 } },
  ];
  const col2 = [
    {
      uuid: "ed92dae2-b45b-4903-a30a-a1587d06147f",
      style: { marginTop: 100, marginLeft: -10 },
    },
    { uuid: "0dbfab2b-1789-4399-a94e-0e94efa54fc7", style: { marginLeft: 10 } },
  ];
  const col3 = [
    { uuid: "081c3d35-27d8-4a9c-ba8f-ac8ab7c7735e", style: {} },
    { uuid: "01f53fe9-9ae7-487b-8362-2f7636050dff", style: {} },
    {
      uuid: "c88e373f-2b6d-48b5-b091-f35e9fcd4681",
      style: { marginTop: 100, marginLeft: -16 },
    },
    {
      uuid: "aa29715a-3b92-4351-a3d8-ba7ea55eddf9",
      style: { marginLeft: -30, marginTop: 20 },
    },
  ];

  let col1Height = margin * (col1.length / 2);
  col1.forEach((el) => {
    col1Height += images[el.uuid].info.art.height;
  });
  let col2Height = margin * (col2.length / 2);
  col2.forEach((el) => {
    col2Height += images[el.uuid].info.art.height;
  });
  let col3Height = margin * (col3.length / 2);
  col3.forEach((el) => {
    col3Height += images[el.uuid].info.art.height;
  });

  let adjustment = Math.max(
    col1Height - height > 0 ? (col1Height - height) / col1.length : 0,
    col2Height - height > 0 ? (col2Height - height) / col2.length : 0,
    col3Height - height > 0 ? (col3Height - height) / col3.length : 0
  );

  height =
    adjustment === 0 ? Math.max(col1Height, col2Height, col3Height) : height;

  return (
    <>
      <SafeAreaView style={styles.container}>
        <Image
          source={require("assets/common/trailoff.png")}
          style={styles.headerImage}
        />
        <View
          style={{
            ...styles.artBox,
            height,
            width,
          }}
        >
          <View style={styles.col}>
            {col1.map((el) => (
              <ImageLink
                key={el.uuid}
                uuid={el.uuid}
                style={el.style}
                adjustment={adjustment}
                margin={margin}
              />
            ))}
          </View>
          <View style={styles.col}>
            {col2.map((el) => (
              <ImageLink
                key={el.uuid}
                uuid={el.uuid}
                style={el.style}
                adjustment={adjustment}
                margin={margin}
              />
            ))}
          </View>
          <View style={styles.col}>
            {col3.map((el) => (
              <ImageLink
                key={el.uuid}
                uuid={el.uuid}
                style={el.style}
                adjustment={adjustment}
                margin={margin}
              />
            ))}
          </View>
        </View>
      </SafeAreaView>
      <View style={styles.footer}>
        <StoryListFooter />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 0,
    backgroundColor: "white",
    width: "100%",
    height: "100%",
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
    marginTop,
  },
  headerImage: {
    marginTop: -headerHeight / 1.6,
    height: headerHeight,
    justifyContent: "center",
    alignItems: "center",
    resizeMode: "contain",
  },
  titleText: {
    justifyContent: "center",
    alignItems: "center",
    color: "#FFFFFF",
  },
  artBox: {
    marginTop: -40,
    zIndex: 0,
    flexDirection: "row",
    justifyContent: "center",
    borderWidth: 0,
  },
  col: {
    flexGrow: 1,
    width: "33%",
    textAlign: "center",
  },
  footer: { position: "absolute", left: 0, right: 0, bottom: 0 },
});
