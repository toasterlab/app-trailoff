import React, { useState, useEffect, useRef } from 'react';
import {
  View,
  SafeAreaView,
  Text,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Image
} from 'react-native';
import Dialog from 'components/dialog';
import { useDispatch, useSelector } from 'react-redux';
import Button from 'components/button';
import TornPaperHeader from 'components/tornPaperHeader';
import fonts from 'styles/fonts';
import Markdown from 'react-universal-markdown/native';
import {
  setCurrentStoryIdx,
  appThemeClipPlay,
  appThemeClipStop,
  appThemeClipDestroy,
  appThemeRestart
} from 'state/actions';

export default ({ route, navigation }) => {
  const dispatch = useDispatch();
  const story = useSelector(state => state.stories[route.params.story.id]);
  const previewSound = `preview_${story.uuid.replace(/-/g, '_')}.mp3`;
  const scrollRef = useRef(null);
  const [isPlaying, setIsPlaying] = useState(false);

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      if (isPlaying) {
        dispatch(appThemeClipDestroy({ sound: previewSound }));
        dispatch(appThemeRestart());
      }
    });
    return unsubscribe;
  }, [navigation, isPlaying]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('state', () => {
      const navState = navigation.dangerouslyGetState();
      const currentStoryId = navState.routes[navState.index].params?.story?.id;
      dispatch(
        setCurrentStoryIdx(
          typeof currentStoryId !== 'undefined' ? currentStoryId : null
        )
      );
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      scrollRef.current.scrollTo({ x: 0, y: 0, animated: true });
    });
    return unsubscribe;
  }, [navigation]);

  return (
    <View style={styles.container}>
      <Dialog />
      <SafeAreaView style={styles.container}>
        <TornPaperHeader story={story} />
        <ScrollView ref={scrollRef}>
          <View style={styles.previewBlock}>
            <View style={styles.previewBlock_left}>
              <TouchableOpacity
                onPress={() => {
                  if (isPlaying) {
                    dispatch(
                      appThemeClipStop({
                        sound: previewSound
                      })
                    );
                  } else {
                    dispatch(
                      appThemeClipPlay({
                        sound: previewSound,
                        solo: true,
                        cb: () => {
                          setIsPlaying(false);
                        }
                      })
                    );
                  }
                  setIsPlaying(!isPlaying);
                }}
              >
                <View
                  style={[
                    styles.playbackIcon,
                    { backgroundColor: story.color }
                  ]}
                >
                  {(isPlaying && (
                    <Image
                      source={require('assets/common/icon/stop.png')}
                      style={styles.play_button}
                    />
                  )) || (
                    <Image
                      source={require('assets/common/icon/play.png')}
                      style={styles.play_button}
                    />
                  )}
                </View>
              </TouchableOpacity>
              <Text style={styles.preview}>Preview</Text>
            </View>
            <View style={styles.previewBlock_right}>
              <Text style={styles.tags}>
                <Text style={styles.header}>Length of trail: </Text>
                {story.detail.length}
              </Text>
              <View>
                <Text style={styles.header}>Trail Notes:</Text>
                <Text style={styles.tags}>
                  {story.detail.notes.map((note, idx) => {
                    return (
                      <Text key={`note_${idx}`}>
                        {note}
                        {idx + 1 < story.detail.notes.length ? ',' : ''}{' '}
                      </Text>
                    );
                  })}
                </Text>
              </View>
              <View>
                <Text style={styles.header}>Tags:</Text>
                <Text style={styles.tags}>
                  {story.detail.tags.map((tag, idx) => {
                    return (
                      <Text key={`tag_${idx}`}>
                        {tag}
                        {idx + 1 < story.detail.tags.length ? ',' : ''}{' '}
                      </Text>
                    );
                  })}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.summaryBlock}>
            <Image
              source={require('assets/common/torn.png')}
              style={[styles.image_paperbg, { tintColor: story.color }]}
            />
            <Text style={styles.summary_header}>Summary:</Text>
            <Markdown
              styles={{
                Text: styles.summaryText,
                Em: styles.summaryTextItalic
              }}
            >
              {story.detail.summary}
            </Markdown>
            <View style={styles.letsGoContainer}>
              <Button
                height={70}
                width={'75%'}
                fontSize={50}
                lineHeight={58}
                onPress={() => {
                  navigation.navigate('StoryLetsGo', {
                    story: { id: story.id, uuid: story.uuid }
                  });
                }}
                text={"Let's Go"}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
      <View style={styles.footer}>
        <View style={styles.footerItem}>
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <Image
              source={require('assets/common/icon/discover.png')}
              style={styles.icon}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.footerItem}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('StoryAuthor', {
                story: { id: story.id, uuid: story.uuid }
              })
            }
          >
            <Image
              source={require('assets/common/icon/author.png')}
              style={styles.icon}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.footerItem}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('StoryCredit', {
                story: { id: story.id, uuid: story.uuid }
              })
            }
          >
            <Image
              source={require('assets/common/icon/credits.png')}
              style={styles.icon}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.footerItem}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('StoryExpand', {
                story: { id: story.id, uuid: story.uuid }
              })
            }
          >
            <Image
              source={require('assets/common/icon/expand.png')}
              style={styles.icon}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  image_paper: {
    position: 'absolute',
    height: 236,
    width: 500,
    resizeMode: 'stretch',
    justifyContent: 'center'
  },
  image_paperbg: {
    position: 'absolute',
    height: 440,
    width: 900,
    marginLeft: -170,
    resizeMode: 'stretch',
    justifyContent: 'center'
  },
  previewBlock: {
    marginTop: 25,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  previewBlock_left: {
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
    height: 200,
    width: 100,
    marginLeft: 30,
    marginRight: 0
  },
  previewBlock_right: {
    justifyContent: 'space-between',
    backgroundColor: 'white',
    flexDirection: 'column',
    minHeight: 180,
    width: 220,
    marginLeft: 16,
    paddingRight: 20
  },
  container: { backgroundColor: 'white', flex: 1 },
  headerRow: { flexDirection: 'row', alignItems: 'center' },
  preview: {
    fontSize: 15,
    marginTop: 7,
    textTransform: 'uppercase',
    fontFamily: fonts.hype_bold
  },
  header: {
    fontWeight: '900',
    fontSize: 15,
    textTransform: 'uppercase',
    fontFamily: fonts.hype_bold
  },
  summaryText: {
    margin: -10,
    fontSize: 18,
    fontWeight: '400',
    fontFamily: fonts.avenir_next_condensed
  },
  summaryTextItalic: {
    fontWeight: '300',
    fontFamily: fonts.avenir_next_condensed_italic
  },
  summary_header: {
    textTransform: 'uppercase',
    fontFamily: fonts.hype,
    fontSize: 30,
    margin: 0,
    marginBottom: -19,
    padding: 0
  },
  tags: {
    fontWeight: '300',
    fontFamily: fonts.avenir_next_condensed,
    fontSize: 13
  },
  bodyCopy: { margin: 20 },
  paragraph: { textAlign: 'center', marginBottom: 20 },
  footer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    paddingBottom: 10,
    backgroundColor: 'white',
    height: 100
  },
  footerItem: {
    margin: 10
  },
  icon: {
    height: 50,
    width: 50,
    margin: 10,
    resizeMode: 'stretch'
  },
  play_button: {
    height: 100,
    width: 100,
    margin: 10,
    resizeMode: 'stretch'
  },
  summaryBlock: {
    paddingTop: 50,
    paddingLeft: 20,
    paddingRight: 20,
    height: 500
  },
  playbackIcon: {
    borderRadius: 70,
    width: 100,
    height: 100,
    padding: 0,
    margin: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  letsGoContainer: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
