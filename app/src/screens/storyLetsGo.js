import React, { useState, useEffect } from 'react';
import BackButton from 'components/backButton';
import {
  View,
  ScrollView,
  SafeAreaView,
  Text,
  Image,
  Alert
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { StyleSheet } from 'react-native';
import fonts from 'styles/fonts';
import images from 'assets/stories/images.js';
import Button from 'components/button';
import BulletList from 'components/bulletList';
import Download from 'components/download';
import { Platform, Linking } from 'react-native';
import { requestPermissions } from 'state/actions';
import { PERMISSIONS, RESULTS } from 'react-native-permissions';
import Dialog from 'components/dialog';
import {
  appDialog,
  playbackReset,
  packageRemove,
  packageDownload,
  appThemePauseAll,
  appThemeClipPlay
} from 'state/actions';

const locationPermission =
  Platform.OS === 'ios'
    ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
    : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;

export default ({ route, navigation }) => {
  const dispatch = useDispatch();
  const currentLocation = useSelector(state => state.app.location);
  const story = useSelector(state => state.stories[route.params.story.id]);

  const storyStatus = useSelector(
    state => state.settings.stories[route.params.story.id].status
  );
  const isDownloading = useSelector(
    state =>
      state.settings.stories[route.params.story.id].download.isDownloading
  );
  const debugMode = useSelector(state => state.settings.app.debugMode);
  const permission = useSelector(
    state => state.app.permissions[locationPermission]
  );
  const isCloseEnough =
    currentLocation !== null && storyStatus?.distanceFromStart * 1000 <= 100;
  const isTrailDownloaded = storyStatus.pkg?.manifest?.package_date;
  const permissionMessage = 'You need to enable location permissions.';

  const manifest = useSelector(state => state.settings.app.manifest);
  const pkgManifest = manifest.latest.find(pkg => pkg.uuid === story.uuid);
  const latestPackageDate = pkgManifest?.package_date;
  if (latestPackageDate && storyStatus.pkg?.manifest) {
    const currentPackageDate = storyStatus.pkg.manifest.package_date;
    const needsUpdate =
      new Date(latestPackageDate) > new Date(currentPackageDate);
    if (needsUpdate && !isDownloading) {
      dispatch(
        appDialog({
          isVisible: true,
          title: story.title,
          description: `There is a newer version of this story available, do you want to download the update?`,
          confirmText: 'Update',
          cancelText: 'No',
          onConfirm: () => {
            dispatch(packageRemove(story.uuid));
            dispatch(packageDownload(story.uuid));
          }
        })
      );
    }
  }

  useEffect(
    () =>
      navigation.addListener('beforeRemove', e => {
        if (!isDownloading) return;

        // Prevent default behavior of leaving the screen
        e.preventDefault();

        // Prompt the user before leaving the screen
        Alert.alert(
          'Cancel download?',
          'The download is not complete. Do you want to cancel it and leave this page?',
          [
            { text: "Don't cancel", style: 'cancel', onPress: () => {} },
            {
              text: 'Cancel',
              style: 'destructive',
              // If the user confirmed, then we dispatch the action we blocked earlier
              // This will continue the action that had triggered the removal of the screen
              onPress: () => navigation.dispatch(e.data.action)
            }
          ]
        );
      }),
    [navigation, isDownloading]
  );

  const isPermissionGranted = permission?.result === RESULTS.GRANTED;
  const onEnableLocation = () => {
    if (permission?.result === RESULTS.BLOCKED) {
      dispatch(
        appDialog({
          isVisible: true,
          title: `Permissions Disabled`,
          description: `You have chosen to disable location permissions, but we need to know where you are to tell you the story! Please fix this under settings and then return here.`,
          cancelText: 'Ok'
        })
      );
    } else {
      dispatch(requestPermissions());
    }
  };

  const onRemove = () => {
    dispatch(
      appDialog({
        isVisible: true,
        title: 'Remove Content?',
        description:
          ' Your progress will not be lost unless you remove the entire app, but you will need to re-download the story if you want to revisit the trail.',
        confirmText: 'Remove',
        cancelText: 'Cancel',
        onConfirm: () => dispatch(packageRemove(story.uuid))
      })
    );
  };

  const onOpenCoordinate = (coordinate, title) => {
    const scheme = Platform.select({
      ios: 'maps:0,0?q=',
      android: 'geo:0,0?q='
    });
    const latLng = `${coordinate.lat},${coordinate.lng}`;
    const label = title;
    const url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`
    });
    Linking.openURL(url);
  };

  return (
    <View style={styles.container}>
      <Dialog />
      <SafeAreaView>
        <View style={styles.headerRow}>
          <View style={styles.backButton}>
            <BackButton icon="back" />
          </View>
          <View style={styles.title}>
            <Text
              adjustsFontSizeToFit={true}
              numberOfLines={1}
              style={styles.titleText}
            >
              {story.trail}
            </Text>
          </View>
        </View>
      </SafeAreaView>
      <ScrollView style={styles.scrollContainer}>
        <View style={styles.mapBlock}>
          <View style={styles.mapImage_container}>
            <Image style={styles.map_image} source={images[story.uuid].map} />
          </View>
          <View style={styles.twoColumn}>
            <View style={styles.twoColumn_col}>
              <Button
                height={34}
                width={110}
                fontSize={20}
                lineHeight={38}
                onPress={() =>
                  onOpenCoordinate(
                    story.detail.parking,
                    `Parking: ${story.title}`
                  )
                }
                text="Parking"
              />
            </View>
            <View style={styles.twoColumn_col}>
              <Button
                height={34}
                width={110}
                fontSize={20}
                lineHeight={38}
                onPress={() =>
                  onOpenCoordinate(
                    story.detail.trailhead,
                    `Trailhead: ${story.title}`
                  )
                }
                text="Trailhead"
              />
            </View>

            {isTrailDownloaded && (
              <View style={styles.twoColumn_col}>
                <Button
                  height={34}
                  width={110}
                  fontSize={20}
                  lineHeight={38}
                  onPress={onRemove}
                  text={'Remove'}
                />
              </View>
            )}
          </View>
        </View>
        <View style={styles.infoBlock}>
          {story.detail.letsgo.parking && (
            <View>
              <Text style={styles.header}>Parking and Transit Info:</Text>
              <BulletList
                keyPrefix={'parking'}
                items={story.detail.letsgo.parking}
              />
            </View>
          )}
          {story.detail.letsgo.notes && (
            <View>
              <Text style={styles.header}>Trail Notes:</Text>
              <BulletList
                keyPrefix={'notes'}
                items={story.detail.letsgo.notes}
              />
            </View>
          )}
        </View>
        <View style={styles.beginBlock}>
          <Image
            source={require('assets/common/torn.png')}
            style={[
              styles.image_paperbg,
              {
                tintColor: story.color
              }
            ]}
          />
          {(!isPermissionGranted && (
            <View style={styles.beginContainer}>
              <Text style={styles.centered}>{permissionMessage}</Text>
              <Button
                height={70}
                width={250}
                fontSize={40}
                onPress={onEnableLocation}
                text={'Enable Location'}
              />
            </View>
          )) ||
            (isTrailDownloaded && (
              <View style={styles.beginContainer}>
                <Text style={styles.centered}>
                  Once you have arrived, put on headphones
                </Text>
                <Button
                  isDisabled={!debugMode && !isCloseEnough}
                  height={70}
                  width={250}
                  fontSize={40}
                  onPress={() => {
                    dispatch(appThemePauseAll());
                    dispatch(
                      appThemeClipPlay({
                        sound: 'theme_letsgo_nkmaster_v01.mp3',
                        isPartOfTheme: false
                      })
                    );
                    dispatch(playbackReset());
                    navigation.navigate('StoryProgress', {
                      story: {
                        id: route.params.story.id,
                        uuid: story.uuid
                      }
                    });
                  }}
                  text={debugMode && !isCloseEnough ? 'SKIP' : 'Begin'}
                />
                {!isCloseEnough && (
                  <Text style={styles.centered}>
                    Not Close Enough! Check Trailhead!
                  </Text>
                )}
              </View>
            )) || (
              <View style={styles.beginContainer}>
                <Text style={styles.centered}>
                  You need to download this trail{'\n'}before you visit
                </Text>
                <Download story={story} />
              </View>
            )}
        </View>
        <View style={styles.padding} />
      </ScrollView>
      {debugMode && (
        <View
          style={{
            position: 'absolute',
            backgroundColor: 'grey',
            bottom: 0,
            height: 90,
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Text>
            Permissions: {isPermissionGranted ? '✅GRANTED' : '🚫NOT Granted'}
          </Text>
          <Text style={{ textAlign: 'center' }}>
            Distance to Start:{' '}
            {storyStatus.distanceFromStart > 1
              ? `${storyStatus.distanceFromStart.toFixed(2)}km`
              : `${(storyStatus.distanceFromStart * 1000).toFixed(2)}m`}
          </Text>
          <Text style={{ textAlign: 'center' }}>
            {currentLocation
              ? `${JSON.stringify(
                  currentLocation.unfiltered
                )}\n ${JSON.stringify(currentLocation.filtered)}`
              : '?'}
          </Text>
        </View>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  backButton: { width: 40, zIndex: 3, elevation: 4 },
  container: {
    flex: 1,
    backgroundColor: 'white',
    height: '100%'
  },
  padding: {
    height: 150
  },
  centered: {
    textAlign: 'center',
    fontWeight: '100',
    fontSize: 24,
    fontFamily: fonts.hype,
    textTransform: 'uppercase',
    margin: 8
  },
  scrollContainer: {
    flexGrow: 1,
    height: '100%',
    flexDirection: 'column'
  },
  header: {
    fontSize: 20,
    marginTop: 15,
    textTransform: 'uppercase',
    fontFamily: fonts.hype_bold
  },
  image_paperbg: {
    position: 'absolute',
    height: 210,
    width: 520,
    marginLeft: -50,
    resizeMode: 'stretch',
    justifyContent: 'center'
  },
  headerRow: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: 0,
    justifyContent: 'center'
  },
  title: {
    marginTop: 10,
    paddingRight: 25,
    flexGrow: 1,
    justifyContent: 'center',
    textAlign: 'center'
  },
  titleText: {
    fontSize: 40,
    margin: 10,
    overflow: 'hidden',
    textTransform: 'uppercase',
    textAlign: 'center',
    fontFamily: fonts.abel,
    marginTop: 0
  },
  mapBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 0,
    padding: 0
  },
  infoBlock: {
    margin: 20,
    marginTop: 20,
    justifyContent: 'space-between',
    marginBottom: 10
  },
  mapImage_container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
    width: '100%',
    marginLeft: 15
  },
  map_image: {
    resizeMode: 'contain'
  },
  twoColumn: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  twoColumn_col: {
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
    width: 100,
    marginLeft: 15
  },
  beginBlock: {
    marginTop: 10,
    marginBottom: 50,
    justifyContent: 'center',
    height: 60,
    flexGrow: 1,
    minHeight: 200
  },
  beginContainer: {
    alignItems: 'center',
    textAlign: 'center'
  }
});
