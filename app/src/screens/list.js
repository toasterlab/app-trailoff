import * as React from 'react';
import BurgerButton from 'components/burgerButton';
import { useNavigation } from '@react-navigation/native';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import fonts from 'styles/fonts';
import images from 'assets/stories/images.js';
import { useSelector } from 'react-redux';
import StoryListFooter from './storyListFooter';
import Dialog from 'components/dialog';

const StoryRow = props => {
  const navigation = useNavigation();
  return (
    <View key={props.story.uuid} style={styles.storyContainer}>
      <Dialog />
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('Story', {
            story: { id: props.story.id, uuid: props.story.uuid }
          })
        }
      >
        <View style={styles.storyInnerContainer}>
          <View style={styles.storyItem}>
            <View style={styles.storyIcon}>
              <Image
                style={styles.storyIconImage}
                source={images[props.story.uuid].icon}
              />
            </View>
            <View style={styles.storyTextContainer}>
              <Text style={styles.storyAuthor}>{props.story.author}</Text>
              <Text style={styles.storyText}>{props.story.title}</Text>
              <Text style={styles.storyText}>{props.story.trail}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default function List({ navigation }) {
  const stories = useSelector(state => state.stories);
  const sorted = stories.sort((a, b) => a.order - b.order);
  return (
    <View style={styles.doc}>
      <View>
        <SafeAreaView style={styles.container}>
          <View style={styles.headerRow}>
            <BurgerButton />
            <View style={styles.title}>
              <Image source={require('assets/common/stories.png')} />
            </View>
          </View>
          <ScrollView style={styles.listContainer}>
            {sorted?.map(story => {
              return <StoryRow key={story.id} story={story} />;
            })}
            <View style={styles.padding} />
          </ScrollView>
        </SafeAreaView>
      </View>
      <StoryListFooter />
    </View>
  );
}

const styles = StyleSheet.create({
  doc: { flex: 1 },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  headerRow: {
    marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    alignItems: 'center',
    flexGrow: 1,
    borderWidth: 0,
    paddingRight: 65
  },
  listContainer: {
    width: '100%'
  },
  storyContainer: {
    height: 100,
    width: '100%',
    marginLeft: 20,
    borderBottomColor: 'grey',
    borderBottomWidth: StyleSheet.hairlineWidth,
    justifyContent: 'center',
    alignItems: 'center'
  },
  padding: {
    height: 130,
    width: '100%'
  },
  storyInnerContainer: {
    height: 100,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  storyItem: { flexDirection: 'row', alignItems: 'center', margin: 20 },
  storyIcon: { width: 80 },
  storyIconImage: { width: 60, height: 60 },
  storyTextContainer: {
    flexGrow: 1,
    flexDirection: 'column',
    fontFamily: fonts.abel
  },
  storyText: {
    fontFamily: fonts.abel,
    fontSize: 20,
    width: 300
  },
  storyAuthor: {
    textTransform: 'uppercase',
    textDecorationLine: 'underline',
    fontFamily: fonts.abel,
    fontSize: 20
  }
});
