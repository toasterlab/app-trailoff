import * as React from 'react';
import {View, SafeAreaView, Text} from 'react-native';

export default () => {
  return (
    <SafeAreaView>
      <View
        style={{
          backgroundColor: 'black',
          height: '100%',
          width: '100%',
        }}
      />
    </SafeAreaView>
  );
};
