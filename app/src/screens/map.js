import React, { useEffect, useState, useCallback } from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import StoryListFooter from './storyListFooter';
import MapboxGL from '@react-native-mapbox-gl/maps';
import { useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import bbox from '@turf/bbox';
import turf from 'turf';
import featurecollection from 'turf-featurecollection';
import images from 'assets/stories/images.js';
MapboxGL.setAccessToken('NO_TOKEN'); //Need to put in something or android will fail

const useMountEffect = f => useEffect(f, []);
const AnnotationContent = props => {
  const navigation = useNavigation();
  return (
    <View
      style={{
        ...styles.storyMarkerContainer,
        borderColor: props.story.color
      }}
    >
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('Story', {
            story: { id: props.story.id, uuid: props.story.uuid }
          })
        }
        style={{
          ...styles.storyMarker,
          backgroundColor: props.story.color
        }}
      >
        <Image
          style={{
            ...styles.mapIcon,
            backgroundColor: props.story.color
          }}
          source={images[props.story.uuid].art}
        />
      </TouchableOpacity>
    </View>
  );
};

export default () => {
  const stories = useSelector(state => state.stories);
  const lastClick = useSelector(state => state.app.lastClick);
  const [bounds, setBounds] = useState({ id: 0, ne: [], sw: [] });

  const setDefaultBounds = useCallback(() => {
    let features = [];
    stories.forEach(story => {
      features.push(
        turf.point([
          parseFloat(story.detail.trailhead.lng),
          parseFloat(story.detail.trailhead.lat)
        ])
      );
    });
    const padding = 50;
    let fc = featurecollection(features);
    let boundingBox = bbox(fc);
    setBounds({
      ne: [boundingBox[0], boundingBox[1]],
      sw: [boundingBox[2], boundingBox[3]],
      id: Math.random(),
      paddingTop: padding,
      paddingLeft: padding,
      paddingRight: padding,
      paddingBottom: padding
    });
  }, [stories]);

  useMountEffect(() => {
    setDefaultBounds();
  });

  useEffect(() => {
    if (lastClick.screen === 'Map' && !lastClick.isInitial) {
      setDefaultBounds();
    }
  }, [lastClick, setDefaultBounds]);

  return (
    <View style={styles.matchParent}>
      <MapboxGL.MapView
        key={bounds.id}
        style={styles.matchParent}
        styleURL="https://assets.trailoff.org/fb979b51-a4d6-48b6-9307-1919bb2fb47c/mapbox.json"
      >
        {stories?.map(story => (
          <MapboxGL.MarkerView
            id={story.uuid}
            key={story.uuid}
            coordinate={[
              parseFloat(story.detail.trailhead.lng),
              parseFloat(story.detail.trailhead.lat)
            ]}
          >
            <AnnotationContent story={story} />
          </MapboxGL.MarkerView>
        ))}
        <MapboxGL.Camera animationDuration={0} bounds={bounds} />
        <MapboxGL.UserLocation />
      </MapboxGL.MapView>
      <StoryListFooter />
    </View>
  );
};
const styles = StyleSheet.create({
  matchParent: {
    flex: 1
  },
  storyMarkerContainer: {
    width: 60,
    height: 60,
    overflow: 'hidden',
    borderRadius: 60,
    borderWidth: 2,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 15,
    shadowOffset: { width: 1, height: 13 }
  },
  storyMarker: {
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  storyButtonText: {
    color: 'white',
    fontWeight: 'bold'
  },
  mapIcon: { height: 100, resizeMode: 'cover' }
});
