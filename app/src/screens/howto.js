import * as React from 'react';
import BurgerButton from 'components/burgerButton';
import {View, SafeAreaView, ScrollView, Text} from 'react-native';
import styles from 'styles/info';

export default () => {
  return (
    <View style={styles.doc}>
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.headerRow}>
            <View style={styles.backButton}>
              <BurgerButton />
            </View>
            <View style={styles.title}>
              <Text style={styles.titleText}>How To</Text>
            </View>
          </View>
          <ScrollView style={styles.bodyCopy}>
            <View style={styles.list}>
              <Text style={styles.header}>A Few Important Reminders:</Text>
              <Text style={styles.paragraph}>• Turn Location Services on</Text>
              <Text style={styles.paragraph}>• Bring headphones with you!</Text>
              <Text style={styles.paragraph}>• Charge your phone</Text>
              <Text style={styles.paragraph}>
                • You walk these trails at your own risk
              </Text>
            </View>
            <View style={styles.list}>
              <Text style={styles.header}>How to use this app:</Text>
              <Text style={styles.paragraph}>
                • Click on a story or trail to explore its features and get an
                audio preview
              </Text>
              <Text style={styles.paragraph}>
                • Hit LET’S GO for directions and trail info
              </Text>
              <Text style={styles.paragraph}>
                • Once at the trail’s start, hit BEGIN
              </Text>
              <Text style={styles.paragraph}>
                • Check out bonus content on the surrounding area after walking the trail
              </Text>
            </View>
            <View style={styles.list}>
              <Text style={styles.header}>Walking the stories:</Text>
              <Text style={styles.paragraph}>
                • Take your time; stories will sync to your movement
              </Text>
              <Text style={styles.paragraph}>
                • If you stray from the path a sound will let you know you’re
                off route
              </Text>
              <Text style={styles.paragraph}>
                •Leave the trail for too long and the Ranger will pause your
                story to pick up again at any time
              </Text>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    </View>
  );
};
