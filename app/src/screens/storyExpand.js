import React, { useState, useEffect } from 'react';
import {
  Platform,
  View,
  SafeAreaView,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
  Linking
} from 'react-native';
import fonts from 'styles/fonts';
import BackButton from 'components/backButton.js';
import images from 'assets/stories/images.js';
import Markdown from 'react-universal-markdown/native';
import { useSelector, useDispatch } from 'react-redux';
import TornPaperHeader from 'components/tornPaperHeader';
import {
  appThemeClipPlay,
  appThemeClipStop,
  appThemeClipDestroy,
  appThemeRestart
} from 'state/actions';
import RNFetchBlob from 'rn-fetch-blob';

let screenHeight = Dimensions.get('window').height;

export default ({ route, navigation }) => {
  const dispatch = useDispatch();
  const story = useSelector(state => state.stories[route.params.story.id]);
  const storyStatus = useSelector(
    state => state.settings.stories[route.params.story.id].status
  );
  const isDownloaded = storyStatus.pkg?.manifest?.package_date;
  const [containerDimensions, setContainerDimensions] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const [isOverlayVisible, setIsOverlayVisible] = useState(false);
  const [overlayContent, setOverlayContent] = useState(null);

  const storyMedia = storyStatus.pkg.media;
  const interviewFileName = `interview_${story.uuid.replace(/-/g, '_')}.mp3`;
  const mediaUrl = storyMedia?.find(item => item.name === interviewFileName)
    ?.url;
  const pathToInterviewAudio = `${RNFetchBlob.fs.dirs.DocumentDir}/package/${story.uuid}/${mediaUrl}`;

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      if (isPlaying) {
        dispatch(appThemeClipDestroy({ sound: 'interview' }));
        dispatch(appThemeRestart());
      }
    });
    return unsubscribe;
  }, [navigation, isPlaying]);

  round = (value, decimals) => {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
  };

  onImagemapClick = e => {
    if (isOverlayVisible) {
      setIsOverlayVisible(false);
      return;
    }
    const originalImage = images[story.uuid].info.expand;
    const ratio = originalImage.width / originalImage.height;

    let scaledImage = { height: 0, width: 0 };
    if (originalImage.height > originalImage.width) {
      scaledImage.height = containerDimensions.height;
      scaledImage.width =
        (containerDimensions.height / originalImage.height) *
        originalImage.width;
    } else {
      scaledImage.width = containerDimensions.width;
      scaledImage.height =
        (containerDimensions.width / originalImage.width) *
        originalImage.height;
    }

    const padding = {
      top: (containerDimensions.height - scaledImage.height) / 2,
      bottom: (containerDimensions.height - scaledImage.height) / 2,
      left: (containerDimensions.width - scaledImage.width) / 2,
      right: (containerDimensions.width - scaledImage.width) / 2
    };

    const containerCoordinates = {
      x: e.nativeEvent.locationX,
      y: e.nativeEvent.locationY
    };

    let mappedX =
      ((containerCoordinates.x - padding.left) / scaledImage.width) *
      originalImage.width;
    let mappedY =
      ((containerCoordinates.y - padding.top) / scaledImage.height) *
      originalImage.height;

    const closest = findClosestTo({ x: mappedX, y: mappedY });
    setOverlayContent(closest);
    if (closest.distance < 30) {
      if (closest.title.toLowerCase() === 'survey') {
        Linking.canOpenURL(closest.url).then(supported => {
          if (supported) {
            Linking.openURL(closest.url);
          } else {
            console.log("Don't know how to open URI: " + closest.url);
          }
        });
      } else {
        setIsOverlayVisible(true);
      }
    }
  };

  findClosestTo = ({ x, y }) => {
    let shortest = 9999999;
    let foundSpot = null;
    story.detail.expand.forEach(spot => {
      const xDiff = x - spot.imageMap.x;
      const yDiff = y - spot.imageMap.y;
      distance = Math.sqrt(xDiff * xDiff + yDiff * yDiff);
      if (distance < shortest) {
        foundSpot = spot;
        shortest = distance;
      }
    });
    return { ...foundSpot, distance: shortest };
  };

  onOpenExpandLink = () => {
    let url = overlayContent.url;
    if (url) {
      Linking.canOpenURL(url).then(supported => {
        if (supported) {
          Linking.openURL(url);
        } else {
          console.log("Don't know how to open URI: " + url);
        }
      });
    } else {
      const scheme = Platform.select({
        ios: 'maps:0,0?q=',
        android: 'geo:0,0?q='
      });
      const latLng = `${overlayContent.coordinates.lat},${overlayContent.coordinates.lng}`;
      const label = overlayContent.title;
      const url = Platform.select({
        ios: `${scheme}${label}@${latLng}`,
        android: `${scheme}${latLng}(${label})`
      });
      Linking.openURL(url);
    }
  };

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.container}>
        <TornPaperHeader story={story} />
        <View style={{ top: -230 }}>
          <BackButton icon="close" />
        </View>
        <View
          style={{
            width: Dimensions.get('window').width,
            textAlign: 'right',
            display: 'flex',
            flexDirection: 'row',
            paddingRight: 20,
            top: -50
          }}
        >
          <View style={{ flexGrow: 1 }} />
          <View style={styles.buttons}>
            <TouchableOpacity
              disabled={!isDownloaded}
              onPress={() => {
                if (isPlaying) {
                  setIsPlaying(false);
                  dispatch(
                    appThemeClipStop({
                      sound: 'interview'
                    })
                  );
                } else {
                  setIsPlaying(true);
                  dispatch(
                    appThemeClipPlay({
                      sound: 'interview',
                      fullPath: pathToInterviewAudio,
                      solo: true,
                      cb: () => {
                        setIsPlaying(false);
                      }
                    })
                  );
                }
              }}
            >
              <View
                style={{
                  ...styles.playbackIcon,
                  backgroundColor: story.color,
                  opacity: isDownloaded ? 1 : 0.5
                }}
              >
                {(isPlaying && (
                  <Image
                    source={require('assets/common/icon/stop.png')}
                    style={styles.play_button}
                  />
                )) || (
                  <Image
                    source={require('assets/common/icon/play.png')}
                    style={styles.play_button}
                  />
                )}
              </View>
            </TouchableOpacity>
            <Text style={styles.preview}>
              {isDownloaded
                ? 'Click to hear an interview with trailoff creator Adrienne Mackey and the Author'
                : 'Click “Let’s Go” and download this story to hear an interview with the author'}
            </Text>
          </View>
        </View>

        <View style={styles.trailMap}>
          <TouchableOpacity onPress={onImagemapClick} activeOpacity={1}>
            <Image
              onLayout={e => {
                setContainerDimensions({
                  height: e.nativeEvent.layout.height,
                  width: e.nativeEvent.layout.width
                });
              }}
              style={{
                height: screenHeight - 310,
                resizeMode: 'contain'
              }}
              source={images[story.uuid].expand}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>

      {isOverlayVisible && (
        <View style={styles.overlay}>
          <TouchableOpacity
            style={styles.overlayInvisibleButton}
            onPress={() => setIsOverlayVisible(false)}
          >
            <View />
          </TouchableOpacity>
          <Image
            source={require('assets/common/contentbg.png')}
            style={styles.overlay_image_paper}
          />
          <TouchableOpacity onPress={onOpenExpandLink}>
            <View
              style={{
                ...styles.expandButton,
                backgroundColor: story.color
              }}
            >
              <Text style={styles.expandButtonText}>
                {overlayContent.title}
              </Text>
            </View>
          </TouchableOpacity>
          <View style={styles.overlayBody}>
            <View>
              <Text style={styles.overlayTitle}>{overlayContent.title}</Text>
              <View style={styles.overlayHrule} />
            </View>
            <ScrollView style={styles.fitParent}>
              <Markdown styles={{ Text: styles.overlayBodyCopy }}>
                {overlayContent.text}
              </Markdown>
            </ScrollView>
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  fitParent: { flexGrow: 1 },
  overlay: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingBottom: 100
  },
  overlayInvisibleButton: {
    marginTop: -30,
    position: 'absolute',
    zIndex: 10,
    elevation: 10,
    height: 50,
    width: '100%',
    opacity: 0.5
  },
  expandButton: {
    marginTop: 20,
    textAlign: 'center'
  },
  expandButtonText: {
    textAlign: 'center',
    fontFamily: fonts.hype_bold,
    textTransform: 'uppercase',
    color: 'black',
    fontSize: 20,
    lineHeight: 65
  },
  overlayBody: {
    padding: 10,
    margin: 18,
    flexGrow: 1,
    bottom: 0,
    height: 300,
    color: 'white'
  },
  overlayTitle: {
    textTransform: 'uppercase',
    color: 'white',
    fontFamily: fonts.hype_bold,
    fontSize: 22
  },
  overlayBodyCopy: {
    color: 'white',
    fontSize: 20,
    fontWeight: '300',
    fontFamily: fonts.avenir_next_condensed
  },
  overlay_image_paper: {
    backgroundColor: 'white',
    position: 'absolute',
    width: 630,
    height: 600,
    marginLeft: -105,
    resizeMode: 'stretch',
    justifyContent: 'center',
    top: -30
  },
  overlayHrule: {
    width: '100%',
    borderWidth: 0.5,
    borderColor: 'white',
    marginTop: 10,
    marginRight: 0,
    marginBottom: 10,
    marginLeft: 0
  },
  titleContainer: {
    marginTop: -50
  },
  trailMap: { position: 'absolute', bottom: 25, left: 5 },
  buttons: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    width: 120,
    right: 0,
    zIndex: 10
  },

  image_paper: {
    position: 'absolute',
    height: 236,
    width: 500,
    resizeMode: 'stretch',
    justifyContent: 'center'
  },
  image_paperbg: {
    position: 'absolute',
    height: 440,
    width: 900,
    marginLeft: -170,
    resizeMode: 'stretch',
    justifyContent: 'center'
  },
  previewBlock: {
    marginTop: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  previewBlock_left: {
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
    height: 200,
    width: 100,
    marginLeft: 15
  },
  previewBlock_right: {
    justifyContent: 'space-between',
    backgroundColor: 'white',
    flexDirection: 'column',
    minHeight: 180,
    width: 270,
    marginLeft: 16,
    marginRight: 10
  },
  titleBlock: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 2,
    marginBottom: 2,
    marginLeft: 25,
    marginRight: 0,
    width: 325,
    height: 240
  },
  surveyButton: { marginTop: 20 },
  container: { backgroundColor: 'white', flex: 1 },
  headerRow: { flexDirection: 'row', alignItems: 'center' },
  title: {
    color: 'white',
    fontFamily: fonts.ostrich_sans_inline,
    fontSize: 40,
    textAlign: 'right'
  },
  trailname: {
    color: 'white',
    fontSize: 20,
    fontFamily: fonts.abel,
    textAlign: 'right',
    textTransform: 'uppercase'
  },
  author: {
    color: 'white',
    fontSize: 20,
    textTransform: 'uppercase',
    textAlign: 'right',
    fontFamily: fonts.abel
  },
  preview: {
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 15,
    marginTop: 7,
    textTransform: 'uppercase',
    fontFamily: fonts.hype_bold
  },
  header: {
    fontSize: 15,
    marginRight: 10,
    textTransform: 'uppercase',
    fontFamily: fonts.hype_bold
  },
  summaryText: {
    margin: -10,
    fontSize: 18,
    fontWeight: '200',
    fontFamily: fonts.avenir_next_condensed
  },
  summaryTextItalic: {
    fontFamily: fonts.avenir_next_condensed_italic
  },
  summary_header: {
    textTransform: 'uppercase',
    fontFamily: fonts.hype,
    fontSize: 30,
    margin: 0,
    marginBottom: -19,
    padding: 0
  },
  hrule: {
    width: 250,
    borderWidth: 0.5,
    borderColor: 'white',
    marginTop: 10,
    marginRight: 0,
    marginBottom: 10,
    marginLeft: 122
  },
  tags: {
    fontWeight: '300',
    fontFamily: fonts.avenir_next_condensed,
    fontSize: 13
  },
  titleText: { textAlign: 'center' },
  bodyCopy: { margin: 20 },
  paragraph: { textAlign: 'center', marginBottom: 20 },
  footer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    paddingBottom: 10,
    height: 100
  },
  footerItem: {
    margin: 10
  },
  icon: {
    height: 50,
    width: 50,
    margin: 10,
    resizeMode: 'stretch'
  },
  play_button: {
    height: 110,
    width: 110,
    margin: 10,
    resizeMode: 'stretch'
  },
  summaryBlock: {
    paddingTop: 50,
    paddingLeft: 20,
    paddingRight: 20,
    height: 500
  },
  playbackIcon: {
    borderRadius: 70,
    width: 100,
    height: 100,
    padding: 0,
    margin: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  letsGoContainer: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

//const expandPoints = [...story.detail.expand];
//let idx = 0;
/*
    // Copy clicked coordinates in
    console.log(`${mappedX}, ${mappedY}`);
    console.log(`IDX: ${idx}`);
    if (idx === expandPoints.length) debugger;
    expandPoints[idx].imageMap = {
      x: Math.round(mappedX),
      y: Math.round(mappedY),
    };
    idx++;
    */
