import BackButton from 'components/backButton.js';
import * as React from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  StyleSheet,
  Image
} from 'react-native';
import fonts from 'styles/fonts';
import images from 'assets/stories/images.js';
import Markdown from 'react-universal-markdown/native';
import { useSelector } from 'react-redux';

export default ({ route, navigation }) => {
  const story = useSelector(state => state.stories[route.params.story.id]);

  return (
    <SafeAreaView style={styles.innerContainer}>
      <BackButton icon="close" />
      <View style={styles.container}>
        <Image
          style={styles.image_headshot}
          source={images[story.uuid].headshot}
        />
        <Text style={styles.author}>{story.author}</Text>
        <ScrollView style={styles.bio}>
          <Markdown styles={{ Text: styles.bioText }}>
            {story.detail.bio}
          </Markdown>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  },
  innerContainer: { flex: 1 },
  image_headshot: { width: 240, height: 240 },
  author: {
    textTransform: 'uppercase',
    fontFamily: fonts.hype,
    fontSize: 35,
    marginTop: 10,
    marginBottom: 0,
    padding: 0
  },
  bio: { width: 350 },
  bioText: {
    fontSize: 15,
    fontWeight: '400',
    fontFamily: fonts.avenir_next_condensed,
    lineHeight: 23
  }
});
