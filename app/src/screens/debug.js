import React, {useState, useEffect} from 'react';
import BurgerButton from 'components/burgerButton';
import Download from 'components/download';
import {View, SafeAreaView, Text} from 'react-native';
import styles from 'styles/info';
import {useSelector} from 'react-redux';
import {useNetInfo} from '@react-native-community/netinfo';
//L: 01f53fe9-9ae7-487b-8362-2f7636050dff
//S: cc4ee685-088b-4899-9dc2-5c24f25fcc9d
export default () => {
  const appState = useSelector(state => state.app);
  const netInfo = useNetInfo();
  return (
    <View style={styles.doc}>
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.headerRow}>
            <BurgerButton />
            <View style={styles.title}>
              <Text style={styles.titleText}>Debug</Text>
            </View>
          </View>
          <View style={styles.bodyCopy}>
            <Download
              story={{
                uuid: 'cc4ee685-088b-4899-9dc2-5c24f25fcc9d',
                color: '#FF00FF',
              }}
            />
            <Download
              story={{
                uuid: '01f53fe9-9ae7-487b-8362-2f7636050dff',
                color: '#FF0000',
              }}
            />
            {appState.network && (
              <>
                <Text>{appState.network.isConnected ? 'OK' : 'NOT OK'}</Text>
                <Text style={{marginTop: 20}}>
                  {JSON.stringify(appState.network)}
                </Text>
              </>
            )}
            <View>
              <Text>Type: {netInfo.type}</Text>
              <Text>Is Connected? {netInfo.isConnected.toString()}</Text>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
};
