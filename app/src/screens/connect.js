import * as React from 'react';
import BurgerButton from 'components/burgerButton';
import {
  View,
  SafeAreaView,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  Linking
} from 'react-native';
import defaultStyles from 'styles/info';
import Link from 'components/link';
import Button from 'components/button';

export default () => {
  const styles = {
    ...defaultStyles,
    icon: {
      borderWidth: 0,
      width: 105,
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      marginBottom: 200
    },
    button: { display: 'flex', alignItems: 'center' },
    link: {
      ...defaultStyles.paragraph,
      fontSize: 30,
      margin: 30,
      marginBottom: 60,
      lineHeight: 40
    }
  };

  onOpenUrl = url => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  };

  return (
    <View style={styles.doc}>
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.headerRow}>
            <View style={styles.backButton}>
              <BurgerButton />
            </View>
            <View style={styles.title}>
              <Text style={styles.titleText}>Connect</Text>
            </View>
          </View>
          <ScrollView style={styles.bodyCopy}>
            <Text style={styles.paragraph}>
              Learn more about TrailOff's creators and development at:
            </Text>
            <Text style={styles.link}>
              <Link url="http://trailoff.org" text="WWW.TRAILOFF.COM" />
            </Text>
            <View style={styles.twoColContainerInner}>
              <View style={styles.icon}>
                <TouchableOpacity
                  onPress={() =>
                    onOpenUrl('https://www.facebook.com/TrailOff/')
                  }
                >
                  <Image source={require('assets/common/logo/facebook.png')} />
                </TouchableOpacity>
              </View>
              <View style={styles.icon}>
                <TouchableOpacity
                  onPress={() =>
                    onOpenUrl('https://www.instagram.com/trailoff_app/')
                  }
                >
                  <Image source={require('assets/common/logo/instagram.png')} />
                </TouchableOpacity>
              </View>
            </View>
            <View>
              <Text style={styles.header}>Help us learn more about you:</Text>
              <View style={styles.button}>
                <Button
                  onPress={() =>
                    onOpenUrl(
                      'https://docs.google.com/forms/d/1UGaeOCHctDSdd8-PUnoGJU8xttiwIotZrSve8F7y3Ig/edit?ts=5e99e71d'
                    )
                  }
                  style={styles.header}
                  height={60}
                  width={'85%'}
                  fontSize={35}
                  text={'Take A Survey'}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    </View>
  );
};
