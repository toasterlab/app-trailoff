import 'react-native-gesture-handler';
import React from 'react';
import Main from 'navigator/main.js';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import { persistor, store } from 'state/store';
import Splash from './screens/splash.js';
import { PersistGate } from 'redux-persist/lib/integration/react';
import EventEmitter from 'eventemitter3';

export default function App() {
  global.event = new EventEmitter();
  return (
    <Provider store={store}>
      <NavigationContainer>
        <PersistGate loading={<Splash />} persistor={persistor}>
          <Main />
        </PersistGate>
      </NavigationContainer>
    </Provider>
  );
}
