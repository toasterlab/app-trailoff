module.exports = {
  colorDim: (color, amt) => {
    let usePound = false;
    if (color[0] === '#') {
      color = color.slice(1);
      usePound = true;
    }

    let num = parseInt(color, 16);
    // eslint-disable-next-line no-bitwise
    let r = (num >> 16) + amt;

    if (r > 255) {
      r = 255;
    } else if (r < 0) {
      r = 0;
    }
    // eslint-disable-next-line no-bitwise
    let b = ((num >> 8) & 0x00ff) + amt;

    if (b > 255) {
      b = 255;
    } else if (b < 0) {
      b = 0;
    }
    // eslint-disable-next-line no-bitwise
    let g = (num & 0x0000ff) + amt;

    if (g > 255) {
      g = 255;
    } else if (g < 0) {
      g = 0;
    }
    // eslint-disable-next-line no-bitwise
    return (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);
  },

  readableFileSize: (bytes, si) => {
    let thresh = si ? 1000 : 1024;
    if (Math.abs(bytes) < thresh) {
      return bytes + ' B';
    }
    let units = si
      ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
      : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    do {
      bytes /= thresh;
      ++u;
    } while (Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1) + ' ' + units[u];
  },

  pad: (n, width, z) => {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
  },
};
