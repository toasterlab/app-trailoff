import * as React from 'react';
import {useNavigation} from '@react-navigation/native';
import {View, TouchableOpacity, Image} from 'react-native';

export default props => {
  const navigation = useNavigation();
  return (
    <View style={{margin: 10, width: 41, zIndex: 3, elevation: 3}}>
      <TouchableOpacity onPress={() => navigation.goBack()} title="Go Back">
        {(props.icon === 'close' && (
          <Image source={require('assets/common/icon/close.png')} />
        )) || <Image source={require('assets/common/icon/back.png')} />}
      </TouchableOpacity>
    </View>
  );
};
