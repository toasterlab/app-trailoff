import React, {Component} from 'react';
import {View, TouchableOpacity, Linking, Text} from 'react-native';
import PropTypes from 'prop-types';

class Progress extends Component {
  render() {
    const style = {
      color: 'black',
      textDecorationLine: 'underline',
      ...this.props.style,
    };
    return (
      <>
        <Text style={style} onPress={() => Linking.openURL(this.props.url)}>
          {this.props.text ? this.props.text : this.props.url}
        </Text>
      </>
    );
  }
}
export default Progress;

Progress.defaultProps = {
  url: '',
};
Progress.propTypes = {
  url: PropTypes.string.isRequired,
  text: PropTypes.string,
};
