import React, { useEffect } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import fonts from 'styles/fonts';
import { useSelector } from 'react-redux';
const buttonWidth = 200;
const buttonHeight = 70;
const buttonFontSize = 37;
import { useDispatch } from 'react-redux';
import { packageDownload, packageDownloadCancel } from 'state/actions';
import { colorDim } from 'util.js';

export default props => {
  const downloadStatus = useSelector(
    state => state.settings.stories[state.app.currentStoryIdx].download
  );
  const dispatch = useDispatch();

  const onButton = () => {
    if (downloadStatus.isDownloading) {
      dispatch(packageDownloadCancel(props.story.uuid));
    } else {
      dispatch(packageDownload(props.story.uuid));
    }
  };

  let backgroundColor = downloadStatus.isDownloading
    ? colorDim(props.story.color, -30)
    : 'transparent';

  let width =
    typeof downloadStatus.progress !== 'undefined'
      ? buttonWidth * (downloadStatus.progress / 100)
      : 0;

  useEffect(() => {
    return () => {
      dispatch(packageDownloadCancel(props.story.uuid));
    };
  }, []);

  return (
    <View style={styles.container}>
      <View
        style={[
          styles.progressBar,
          {
            backgroundColor,
            width
          }
        ]}
      />
      <View style={styles.center}>
        <TouchableOpacity onPress={onButton}>
          <Text style={styles.buttonText}>
            {downloadStatus.isDownloading
              ? `cancel • ${downloadStatus.progress}%`
              : 'download'}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    backgroundColor: 'black',
    color: 'white',
    borderRadius: 40,
    height: buttonHeight,
    width: buttonWidth
  },
  progressBar: {
    position: 'absolute',
    height: buttonHeight
  },
  buttonText: {
    color: 'white',
    textTransform: 'uppercase',
    fontFamily: buttonHeight <= 30 ? fonts.hype_bold : fonts.hype,
    fontSize: buttonFontSize,
    margin: 0,
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
    lineHeight: buttonFontSize * 1.96
  },
  center: { alignItems: 'center', justifyContent: 'center' }
});
