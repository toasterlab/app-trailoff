import * as React from 'react';
import {StyleSheet, View} from 'react-native';

export default props => {
  let dots = [];
  for (let i = 1; i <= props.numberOfDots; i++) {
    const style = i === props.selected ? styles.selectedDot : styles.dot;
    dots.push(<View key={i} style={style} />);
  }
  return <View style={styles.dotRow}>{dots}</View>;
};
const styles = StyleSheet.create({
  dotRow: {display: 'flex', flexDirection: 'row'},
  selectedDot: {
    backgroundColor: 'black',
    height: 8,
    width: 8,
    marginRight: 10,
    borderRadius: 8,
  },
  dot: {
    backgroundColor: 'lightgrey',
    height: 8,
    width: 8,
    marginRight: 10,
    borderRadius: 8,
  },
});
