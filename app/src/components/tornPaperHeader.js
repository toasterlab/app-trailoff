import React, { useState, useEffect, useRef } from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import fonts from 'styles/fonts';

export default ({ story }) => {
  return (
    <View style={styles.container}>
      <Image
        source={require('assets/common/tornpaper.png')}
        style={styles.image_paper}
      />
      <View style={styles.titleBlock}>
        <View
          style={{
            textAlign: 'right',
            width: Dimensions.get('window').width,
            paddingRight: 20,
            paddingLeft: 50
          }}
        >
          <Text style={styles.title}>{story.title}</Text>
          <View style={styles.hrule} />
          <Text style={styles.trailname}>{story.trail}</Text>
          <View style={styles.hrule} />
          <Text style={styles.author}>{story.author}</Text>
          <Text style={styles.coauthor}>{story.coauthor}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 0
  },
  image_paper: {
    position: 'absolute',
    height: 236,
    width: 500,
    resizeMode: 'stretch',
    justifyContent: 'center'
  },
  titleBlock: {
    marginTop: 25,
    height: 200,
    justifyContent: 'center'
  },
  title: {
    color: 'white',
    fontFamily: fonts.ostrich_sans_inline,
    fontSize: 40,
    lineHeight: 40,
    textAlign: 'right'
  },
  trailname: {
    color: 'white',
    fontSize: 20,
    lineHeight: 30,
    fontFamily: fonts.abel,
    textAlign: 'right',
    textTransform: 'uppercase'
  },
  author: {
    color: 'white',
    fontSize: 20,
    textTransform: 'uppercase',
    textAlign: 'right',
    fontFamily: fonts.abel
  },
  coauthor: {
    color: 'white',
    fontSize: 15,
    textTransform: 'uppercase',
    textAlign: 'right',
    fontFamily: fonts.abel
  },
  hrule: {
    width: 250,
    borderWidth: 0.5,
    borderColor: 'white',
    marginTop: 10,
    marginRight: 0,
    marginBottom: 10,
    marginLeft: 122
  }
});
