import React, { useState, useEffect } from 'react';
import email from 'react-native-email';
import { View, Text, TouchableOpacity } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { StyleSheet } from 'react-native';
import Markdown from 'react-universal-markdown/native';
import { ScrollView } from 'react-native-gesture-handler';
import {
  toggleFakeLocation,
  updateFakeLocation,
  logReset,
  playbackReset,
  playbackToggleOneway
} from '../state/actions';
import AUDIOTOUR_CONST from 'audioTourConstants.js';

export default props => {
  const dispatch = useDispatch();
  const appState = useSelector(state => state.app);
  const storyStatus = useSelector(
    state => state.settings.stories[props.story.id].status
  );
  const backgroundColor = appState.isLocationFake ? 'orange' : '#BADA55';
  const [poiPointer, setPoiPointer] = useState(0);
  const [locationArray, setLocationArray] = useState(storyStatus.pkg?.poi);
  const sessionId = useSelector(state => state.app.sessionId);
  const analytics = useSelector(state => state.log.analytics);
  const globalVisitCount = analytics ? Object.keys(analytics).length : 0;
  const oneWay = useSelector(state => state.playback.oneWay);
  let triggerVisitCount = 0;

  useEffect(() => {
    let newLocArr = [];
    newLocArr.push({
      coordinate: AUDIOTOUR_CONST.FAR_AWAY,
      sortorder: -1,
      name: 'THE FAR AWAY',
      id: 0
    });
    newLocArr = [...newLocArr, ...storyStatus.pkg?.poi];
    setLocationArray(newLocArr);
    dispatch(
      updateFakeLocation({
        latitude: newLocArr[0].coordinate.lat,
        longitude: newLocArr[0].coordinate.lng
      })
    );
  }, [storyStatus.pkg?.poi]);

  const sessions =
    analytics && storyStatus.inside
      ? analytics[props.story.uuid]
        ? analytics[props.story.uuid][storyStatus.inside?.id]
        : null
      : null;
  if (sessions)
    Object.keys(sessions).forEach(key => (triggerVisitCount += sessions[key]));

  const setPointer = pointer => {
    setPoiPointer(pointer);
    dispatch(
      updateFakeLocation({
        latitude: locationArray[pointer].coordinate.lat,
        longitude: locationArray[pointer].coordinate.lng
      })
    );
  };

  const log = appState.debug.log?.length > 0 ? appState.debug.log : sessionId;
  return (
    <View style={styles.debug_container}>
      {(appState.isLocationFake && (
        <View style={styles.debug_container}>
          <View style={styles.sessionId}>
            <View
              style={{
                flexGrow: 1,
                textAlign: 'center',
                justifyContent: 'center',
                marginLeft: 5
              }}
            >
              <Text>{sessionId}</Text>
            </View>
            <View>
              <TouchableOpacity
                style={{
                  ...styles.location,
                  backgroundColor
                }}
                onPress={() => {
                  const body = `${sessionId}\n\n${appState?.debug.log}`;
                  email(['a@feralresearch.org'], {
                    subject: 'Trailoff Log',
                    body
                  }).catch(console.error);
                }}
              >
                <View style={styles.button}>
                  <Text style={styles.iconButton}>✉️</Text>
                  <Text style={styles.iconButton}>Mail Log</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <ScrollView
            style={{
              height: 100,
              backgroundColor: 'grey',
              width: '100%',
              margin: 0
            }}
          >
            <Markdown styles={{ Text: { fontSize: 10 } }}>
              {log !== null && log && log.length > 0 ? log : ''}
            </Markdown>
          </ScrollView>
          <View style={{ display: 'flex', flexDirection: 'row' }}>
            <View style={{ flexGrow: 1 }}>
              <TouchableOpacity
                style={{
                  ...styles.location,
                  backgroundColor
                }}
                onPress={() => dispatch(toggleFakeLocation())}
              >
                <View>
                  <Text
                    style={styles.centerText}
                  >{`${appState.location?.latitude},${appState.location?.longitude}`}</Text>
                  <Text style={styles.centerText}>
                    {locationArray[poiPointer].id > 0
                      ? `(${locationArray[poiPointer].id}) ${locationArray[poiPointer].name} (T:${globalVisitCount} L:${triggerVisitCount})`
                      : `${locationArray[poiPointer].name}`}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => {
                dispatch(playbackReset());
                const pointer = 0;
                setPointer(pointer);
              }}
            >
              <View style={styles.button}>
                <Text style={styles.iconButton}>🔄</Text>
                <Text style={styles.iconButton}>Session </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => dispatch(logReset(props.story.uuid))}
            >
              <View style={styles.button}>
                <Text style={styles.iconButton}>🔄</Text>
                <Text style={styles.iconButton}>Counters</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => dispatch(playbackToggleOneway())}>
              <View
                style={{
                  ...styles.button,
                  backgroundColor: oneWay ? 'red' : 'green'
                }}
              >
                <Text style={styles.iconButton}>🔙</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.debug_buttons}>
            <TouchableOpacity
              style={styles.debug_button}
              onPress={() => {
                const pointer = poiPointer - 1 >= 0 ? poiPointer - 1 : 0;
                setPointer(pointer);
              }}
            >
              <View>
                <Text style={{ fontSize: 25 }}>{poiPointer > 0 && '⬅️'}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.debug_button}
              onPress={() => {
                const pointer = 0;
                setPointer(pointer);
              }}
            >
              <View>
                <Text style={{ fontSize: 25 }}>↪️</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.debug_button}
              onPress={() => {
                const next = poiPointer + 1;
                setPointer(
                  next >= storyStatus.pkg.poi.length
                    ? storyStatus.pkg.poi.length
                    : next
                );
              }}
            >
              <View>
                <Text style={{ fontSize: 25 }}>
                  {poiPointer < storyStatus.pkg.poi.length && '➡️'}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      )) || (
        <View style={styles.debug_container}>
          <View style={styles.sessionId}>
            <Text>{sessionId}</Text>
          </View>
          <ScrollView
            style={{
              height: 100,
              backgroundColor: 'grey',
              width: '100%',
              margin: 0
            }}
          >
            <Markdown styles={{ Text: { fontSize: 10 } }}>
              {appState.debug.log ? appState.debug.log : 'NO LOG'}
            </Markdown>
          </ScrollView>
          <TouchableOpacity
            style={{
              ...styles.location,
              backgroundColor
            }}
            onPress={() => dispatch(toggleFakeLocation())}
          >
            <View>
              <Text
                style={styles.centerText}
              >{`${appState.location?.latitude},${appState.location?.longitude}`}</Text>
            </View>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  location: {
    backgroundColor: '#BADA55',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  sessionId: {
    backgroundColor: '#eeeeee',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row'
  },
  iconButton: {
    textAlign: 'center',
    color: 'white',
    fontSize: 10
  },
  debug_container: {
    position: 'absolute',
    display: 'flex',
    flexDirection: 'column',
    bottom: 0,
    opacity: 0.9,
    height: 300,
    right: 0,
    left: 0,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center'
  },
  button: {
    justifyContent: 'center',
    borderWidth: 1,
    backgroundColor: '#222222',
    width: 50,
    height: 50
  },

  debug_buttons: {
    display: 'flex',
    flexDirection: 'row',
    bottom: 0,
    opacity: 0.5,
    height: 50,
    right: 0,
    left: 0,
    textAlign: 'center',
    backgroundColor: 'grey',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  debug_button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    textAlign: 'center',
    width: 150,
    height: '100%'
  },
  centerText: {
    textAlign: 'center'
  }
});
