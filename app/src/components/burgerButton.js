import * as React from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, TouchableOpacity, Image } from 'react-native';
import { useDispatch } from 'react-redux';
import { appThemeClipPlay } from 'state/actions';

export default () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  return (
    <View style={{ margin: 10, width: 41, zIndex: 3, elevation: 3 }}>
      <TouchableOpacity
        onPress={() => {
          dispatch(
            appThemeClipPlay({
              sound: 'trailoff_hamburgermenububbles_nkmaster_v01.mp3',
              isPartOfTheme: true
            })
          );
          navigation.toggleDrawer();
        }}
        title="Hamburger"
      >
        <Image source={require('assets/common/icon/burger.png')} />
      </TouchableOpacity>
    </View>
  );
};
