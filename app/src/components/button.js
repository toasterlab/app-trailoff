import * as React from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import fonts from 'styles/fonts';

export default props => {
  return (
    <View
      style={{
        ...styles.button,
        ...props.style,
        opacity: props.isDisabled ? 0.5 : 1,
        height: props.height,
        width: props.width,
      }}>
      <TouchableOpacity disabled={props.isDisabled} onPress={props.onPress}>
        <Text
          style={{
            ...styles.buttonText,
            fontFamily: props.height <= 30 ? fonts.hype_bold : fonts.hype,
            fontSize: props.fontSize,
            lineHeight: props.lineHeight
              ? props.lineHeight
              : props.fontSize * 1.9,
          }}>
          {props.text}
        </Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  button: {
    backgroundColor: 'black',
    color: 'white',
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    textTransform: 'uppercase',
    margin: 0,
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
