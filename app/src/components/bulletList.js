import * as React from 'react';
import Markdown from 'react-universal-markdown/native';
import { View, Text, StyleSheet } from 'react-native';
import fonts from 'styles/fonts';

export default props => {
  let list = [];
  props.items.forEach((item, idx) => {
    list.push(
      <View key={`${props.keyPrefix}_${idx}`} style={styles.item}>
        <View style={styles.bullet}>
          <Text>•</Text>
        </View>
        <Markdown
          styles={{
            Paragraph: { margin: 0, padding: 0 },
            Document: { margin: 0, padding: 0 },
            Text: { margin: 0, fontFamily: fonts.avenir_next_condensed }
          }}
        >
          {item}
        </Markdown>
      </View>
    );
  });
  return list;
};
const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    marginBottom: -14,
    marginTop: -15,
    padding: 0,
    alignItems: 'center'
  },
  bullet: {
    marginRight: 2
  }
});
