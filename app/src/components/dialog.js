import React, { useState } from 'react';
import Dialog from 'react-native-dialog';
import { View } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { appDialog } from 'state/actions';

export default () => {
  const dispatch = useDispatch();
  const dialog = useSelector(state => state.app.dialog);
  return (
    <View>
      <Dialog.Container visible={dialog.isVisible}>
        <Dialog.Title>{dialog.title ? dialog.title : 'Alert'}</Dialog.Title>
        <Dialog.Description>
          {dialog.description ? dialog.description : 'Unknown'}
        </Dialog.Description>
        <Dialog.Button
          label={dialog.cancelText ? dialog.cancelText : 'OK'}
          onPress={() => dispatch(appDialog({ isVisible: false }))}
        />
        {dialog.confirmText && (
          <Dialog.Button
            label={dialog.confirmText}
            onPress={() => {
              dialog.onConfirm();
              dispatch(appDialog({ isVisible: false }));
            }}
          />
        )}
      </Dialog.Container>
    </View>
  );
};
