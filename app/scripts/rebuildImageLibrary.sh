#!/bin/bash
idx=0
cd ./src/assets/stories
rm images.js;
rm images_info.js
for f in *; do
    if [ -d ${f} ]; then
        echo "Processing $f..."
        if [ "$idx" -eq "0" ]; then
          printf "// THIS IS A GENERATED FILE, DO NOT EDIT\n// (run yarn rebuildImageLibrary instead)\n\n" >> images.js
        fi
        printf "import i_$idx from './$f/images.js';\n" >> images.js
        cd $f
        printf "// THIS IS A GENERATED FILE, DO NOT EDIT\n// (run yarn rebuildImageLibrary instead)\n\n" > images.js
        printf "import info from './images_info.js';\n" >> images.js
        printf "export default {\n" >> images.js
        printf "export default {\n" > images_info.js;
        printf "  info,\n" >> images.js
        for i in *@3x.png; do
          printf "  ${i/@3x.png/}: require('./${i/@3x/}'),\n" >> images.js
          printf "  ${i/@3x.png/}: " >> images_info.js
          convert ${i/@3x.png/}.png -ping -format "{width: %w, height: %h}" info:  >> images_info.js
          printf ",\n"  >> images_info.js
        done
        printf "};\n" >> images_info.js;
        printf "};\n" >> images.js

        cd ..
        ((idx=idx+1))
    fi
done
idx=0
printf "\nexport default {\n" >> images.js
for f in *; do
    if [ -d ${f} ]; then
        printf "  '$f': i_$idx,\n" >> images.js
        ((idx=idx+1))
    fi
done
printf "};\n" >> images.js
