#!/bin/bash
idx=0
cd ./src/assets/stories
rm sounds.js;
shopt -s nullglob
for f in *; do
    if [ -d ${f} ]; then
        echo "Processing $f..."
        if [ "$idx" -eq "0" ]; then
          printf "// THIS IS A GENERATED FILE, DO NOT EDIT\n// (run yarn rebuildSoundLibrary instead)\n\n" >> sounds.js
        fi
        printf "import s_$idx from './$f/sounds.js';\n" >> sounds.js
        cd $f
        printf "// THIS IS A GENERATED FILE, DO NOT EDIT\n// (run yarn rebuildSoundLibrary instead)\n\n" > sounds.js
        printf "export default {\n" >> sounds.js
        for i in *.{wav,mp3}; do
          filename=$(basename -- "$i")
          printf "  ${filename%.*}: require('./${i}'),\n" >> sounds.js
        done
        printf "};\n" >> sounds.js
        cd ..
        ((idx=idx+1))
    fi
done
idx=0
printf "\nexport default {\n" >> sounds.js
for f in *; do
    if [ -d ${f} ]; then
        printf "  '$f': s_$idx,\n" >> sounds.js
        ((idx=idx+1))
    fi
done
printf "};\n" >> sounds.js
shopt -u nullglob
