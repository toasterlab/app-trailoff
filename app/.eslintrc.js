module.exports = {
  root: true,
  extends: '@react-native-community',

  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error',
    'no-debugger': 'off',
    curly: 'multi-or-nest',
    'comma-dangle': ['error', 'never'],
  },
};
