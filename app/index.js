/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as appName } from './src/app.json';
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Require cycle:']);
if (__DEV__) {
  // This is a workaround to supress annoying requirecycle warnings:
  // https://github.com/facebook/metro/issues/287
  const IGNORED_WARNINGS = [
    'Remote debugger is in a background tab which may cause apps to perform slowly',
    'Require cycle:'
  ];
  const oldConsoleWarn = console.warn;

  console.warn = (...args) => {
    if (
      typeof args[0] === 'string' &&
      IGNORED_WARNINGS.some(ignoredWarning =>
        args[0].startsWith(ignoredWarning)
      )
    ) {
      return;
    }

    return oldConsoleWarn.apply(console, args);
  };
}
AppRegistry.registerComponent(appName, () => App);
