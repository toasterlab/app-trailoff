# Trailoff App

# Version : 2.0.5 (First Release)

app
Contains the React Native sourcecode

Font corrections:
// https://hybridheroes.de/blog/2019-03-29-react-native-fonts/

# PRODUCTION BUILD:

- IOS: In info.plist, under App Transport Security Settings you MUST set `Allow Arbitrary Loads` to NO for production and YES for development

- Android:

# Assets

Cross-platform assets on RN are (still) a giant pain in the ass. Here's how it works in this project:

## Fonts

- Fonts would work using `rn link` alone, but we also want to support sound, so I moved to using [react-native-asset](https://www.npmjs.com/package/react-native-asset)
- located in app/assets, linked in via `react-native.config.js.`
- Once the fonts are in, they are referred to differently by android and ios, to resolve this see `/src/styles/fonts.js`
- Files should: start with a letter, be lowercase, contain only characters and underscores (no underscores)

## Sounds

- The issue is that RN .6 allows us to statically link files the way we do for images, but react-native-sound cannot access them for some reason. Therefore
  we are obligated to link in sounds using a different mechanism.
- Linked via [react-native-asset](https://www.npmjs.com/package/react-native-asset)
- Located in app/assets, linked in via `react-native.config.js`
- Files should: start with a letter, be lowercase, contain only characters and underscores (no underscores)
- To access the sounds, use rn-fetch-blob to read from the asset directories, which are different on each platform.

## Images

- Located under src/assets/...
- it is necessary to link these in at compile time, there is a support script called via 'yarn rebuildImageLibrary' which will set up the include
  files for you, this handles the ./story subidrectory, the others are linked in one at a time as needed.

## Build for Release

IOS:

- Need real device, work in Xcode
- In info.plist 'Allow Arbitrary Loads" false
- Make sure version is correct
- Archive, then use [App Store Connect](https://appstoreconnect.apple.com/)

Android:

- https://reactnative.dev/docs/signed-apk-android
- Key in 1pass if missing
- https://play.google.com/console/about/
- APK: /android/app/build/outputs/bundle/release/app-release.aab

MYAPP_UPLOAD_STORE_FILE=trailoff-app/app/android/app/my-upload-key.keystore
MYAPP_UPLOAD_KEY_ALIAS=my-key-alias
MYAPP_UPLOAD_STORE_PASSWORD=
MYAPP_UPLOAD_KEY_PASSWORD=

./gradlew bundleRelease
